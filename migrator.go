package dfmigrate

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"log/slog"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/pureharvest/df32"
	"golang.org/x/text/transform"

	"github.com/andybalholm/crlf"
	"github.com/pkg/errors"
)

const (
	MirgationPathKey      = "migrationsPath"
	FileDefinitionPathKey = "fileDefinitionPath"
	DriverKey             = "driver"
	URLKey                = "url"
	FilelistKey           = "filelist"
)

type Migrator struct {
	cfg     Options
	driver  Driver
	drivers map[string]Driver
	env     df32.Environment
	Log     *slog.Logger
}

func (m *Migrator) Register(name string, driver Driver) error {
	if m.drivers == nil {
		m.drivers = map[string]Driver{}
	}

	m.drivers[name] = driver

	return nil
}

func (m *Migrator) Initialize(options Options) error {
	m.cfg = options

	driverName := options.Driver

	if _, found := m.drivers[driverName]; !found {
		return fmt.Errorf("driver %s not found", driverName)
	}

	m.driver = m.drivers[driverName]

	if m.Log == nil {
		m.Log = slog.New(slog.NewTextHandler(io.Discard, nil))
	}

	m.driver.SetLogger(m.Log)

	return nil
}

func (m *Migrator) GetNextVersion() (uint64, error) {
	var (
		files MigrationFiles
		err   error
	)

	if files, err = ReadMigrationFiles(m.cfg.MigrationsPath); err != nil {
		return 0, errors.Wrap(err, "error reading migrations")
	}

	if len(files) == 0 {
		return 1, nil
	}

	return files[len(files)-1].Version + 1, nil
}

func (m *Migrator) Bootstrap() error {
	filelistPath := m.cfg.FilelistPath
	if _, err := os.Stat(filelistPath); os.IsNotExist(err) {
		filelist, err := df32.NewFilelist(filelistPath)
		if err != nil {
			return err
		}

		if err := filelist.Save(); err != nil {
			return err
		}
	}

	return m.driver.Bootstrap(m.cfg.URL)

}

func (m *Migrator) Baseline(version uint64) error {

	if err := m.driver.Initialize(m.cfg); err != nil {
		return err
	}

	return m.driver.Baseline(version)
}

func (m *Migrator) Create(name string) (MigrationFile, error) {
	nextVersion, err := m.GetNextVersion()

	if err != nil {
		return MigrationFile{}, errors.Wrap(err, "could not get next version")
	}

	migration := MigrationFile{
		Version: nextVersion,
		UpMigration: &Migration{
			Name:      name,
			Direction: Up,
			Version:   nextVersion,
		},
		DownMigration: &Migration{
			Name:      name,
			Direction: Down,
			Version:   nextVersion,
		},
	}
	return migration, nil
}

func (m *Migrator) runMigrations(migrations []Migration) error {
	if len(migrations) > 0 {
		for _, migration := range migrations {
			if err := m.driver.Apply(migration); err != nil {
				return err
			}
		}
	}

	return nil
}

func (m *Migrator) Up() error {

	if err := m.driver.Initialize(m.cfg); err != nil {
		return err
	}

	defer m.Close()

	var (
		files   MigrationFiles
		version uint64
		err     error
	)

	if files, err = ReadMigrationFiles(m.cfg.MigrationsPath); err != nil {
		return errors.Wrap(err, "error reading migrations")
	}

	if version, err = m.Version(); err != nil {
		return errors.Wrap(err, "error reading version")
	}

	applyMigrations, err := files.ToLastFrom(version)

	if err != nil {
		return errors.Wrap(err, "error loading migrations")
	}

	if err := m.runMigrations(applyMigrations); err != nil {
		return errors.Wrap(err, "error running migrations")
	}

	return nil
}

func (m *Migrator) Down() error {

	if err := m.driver.Initialize(m.cfg); err != nil {
		return err
	}

	var (
		files   MigrationFiles
		version uint64
		err     error
	)

	if version, err = m.Version(); err != nil {
		return errors.Wrap(err, "error reading version")
	}

	if files, err = ReadMigrationFiles(m.cfg.MigrationsPath); err != nil {
		return errors.Wrap(err, "error reading migrations")
	}

	applyMigrations, err := files.ToFirstFrom(version)

	if err != nil {
		return errors.Wrap(err, "error loading migrations")
	}

	if err := m.runMigrations(applyMigrations); err != nil {
		return errors.Wrap(err, "error running migrations")
	}

	return nil
}

func (m *Migrator) Redo() error {
	if err := m.driver.Initialize(m.cfg); err != nil {
		return err
	}

	var (
		files   MigrationFiles
		current uint64
		err     error
	)

	if current, err = m.Version(); err != nil {
		return err
	}

	if files, err = ReadMigrationFiles(m.cfg.MigrationsPath); err != nil {
		return err
	}

	downMigrations, err := files.From(current, current-1)
	m.Log.Info(fmt.Sprintf("Migrating from %d to %d. %d of %d migrations", current, current-1, len(downMigrations), files.Len()))
	if err != nil {
		return err
	}

	if err := m.runMigrations(downMigrations); err != nil {
		return errors.Wrap(err, "error running migrations")
	}

	upMigrations, err := files.From(current-1, current)
	m.Log.Info(fmt.Sprintf("Migrating from %d to %d. %d of %d migrations", current-1, current, len(upMigrations), files.Len()))
	if err != nil {
		return err
	}

	if err := m.runMigrations(upMigrations); err != nil {
		return errors.Wrap(err, "error running migrations")
	}

	if m.cfg.FileDefinitionsPath == "" {
		return nil
	}

	return nil
}

func (m *Migrator) Goto(version uint64) error {

	if err := m.driver.Initialize(m.cfg); err != nil {
		return err
	}

	var (
		files   MigrationFiles
		current uint64
		err     error
	)

	if current, err = m.Version(); err != nil {
		return err
	}

	if files, err = ReadMigrationFiles(m.cfg.MigrationsPath); err != nil {
		return err
	}

	applyMigrations, err := files.From(current, version)
	m.Log.Info(fmt.Sprintf("Migrating from %d to %d. %d of %d migrations", current, version, len(applyMigrations), files.Len()))
	if err != nil {
		return err
	}

	if err := m.runMigrations(applyMigrations); err != nil {
		return errors.Wrap(err, "error running migrations")
	}

	if m.cfg.FileDefinitionsPath == "" {
		return nil
	}

	return nil
}

func (m *Migrator) Verify(version uint64) error {

	if err := m.driver.Initialize(m.cfg); err != nil {
		return err
	}

	if version == 0 {
		files, err := ReadMigrationFiles(m.cfg.MigrationsPath)

		if err != nil {
			return err
		}

		for _, file := range files {
			if file.Version > version {
				version = file.Version
			}
		}
	}

	var current, expected Model
	var err error

	if current, err = m.driver.Model(); err != nil {
		return err
	}

	if expected, err = buildInMemoryModel(m.cfg.MigrationsPath, version); err != nil {
		return err
	}

	return expected.Compare(current)
}

func (m *Migrator) Generate(version uint64) error {

	model, err := buildInMemoryModel(m.cfg.MigrationsPath, version)

	if err != nil {
		return errors.Wrap(err, "couldn't build in memory model")
	}

	log.Println("generating file definitions")

	if err := m.generateFileDefinitions(model); err != nil {
		return errors.Wrap(err, "could not generate file definitions")
	}

	log.Println("generating driver files")
	if err := m.driver.GenerateFiles(m.cfg, model); err != nil {
		return errors.Wrap(err, "couldn't generate driver files")
	}

	list, err := df32.NewFilelist(m.cfg.FilelistPath)

	if err != nil {
		return errors.Wrap(err, "couldn''t create filelist")
	}

	for _, table := range model.Tables() {
		entry := df32.FilelistEntry{
			Number:       table.Number,
			DisplayName:  table.DisplayName,
			Name:         table.Name,
			DataflexName: table.Name,
			Driver:       "OTHER",
		}

		if err := list.Set(entry); err != nil {
			return errors.Wrap(err, "couldn't set filelist entry")
		}
	}

	return list.Save()
}

func (m *Migrator) GenerateFileDefinitions(version uint64) error {
	if err := m.driver.Initialize(m.cfg); err != nil {
		return err
	}

	model, err := buildInMemoryModel(m.cfg.MigrationsPath, version)

	if err != nil {
		return errors.Wrap(err, "could not generate model")
	}

	if err := m.generateFileDefinitions(model); err != nil {
		return errors.Wrap(err, "could not generate file definitions")
	}

	log.Println("finished updating file definitions")

	return nil
}

func (m *Migrator) generateFileDefinitions(model Model) error {

	rootDir, err := os.Stat(m.cfg.FileDefinitionsPath)

	if err != nil {
		return err
	}

	if !rootDir.IsDir() {
		return errors.Errorf("%s is not a directory", m.cfg.FileDefinitionsPath)
	}

	list, err := ioutil.ReadDir(m.cfg.FileDefinitionsPath)

	if err != nil {
		return err
	}

	for _, file := range list {
		if strings.HasSuffix(strings.ToLower(file.Name()), ".fd") && !strings.EqualFold(file.Name(), "flexerrs.fd") {
			if err := os.Remove(filepath.Join(m.cfg.FileDefinitionsPath, file.Name())); err != nil {
				return err
			}
		}

	}

	for _, table := range model.Tables() {
		fdPath := filepath.Join(m.cfg.FileDefinitionsPath, strings.ToLower(table.Name)+".fd")
		m.Log.Info(fmt.Sprintf("writing file definition for %s to %s...", table.Name, fdPath))

		fdFile, err := os.Create(fdPath)
		if err != nil {
			return err
		}
		defer fdFile.Close()

		t := transform.NewWriter(fdFile, crlf.ToCRLF{})

		if err := table.WriteFileDefinition(t); err != nil {
			return err
		}
		log.Println("done.")
	}

	return nil
}

func (m *Migrator) Version() (uint64, error) {

	if err := m.driver.Initialize(m.cfg); err != nil {
		return 0, err
	}

	return m.driver.Version()
}

func (m *Migrator) Close() error {
	if m.driver != nil {
		return m.driver.Close()
	}
	return nil

}
