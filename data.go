package dfmigrate

type DataMigration struct {
	Format     string
	CSVHeaders bool `json:"csv_headers"`
	Filename   string
	Data       string
	Table      string
	Number     int `json:"file_number"`
}
