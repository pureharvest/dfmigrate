package dfmigrate

import (
	"errors"
	"log/slog"
	"strings"

	"gitlab.com/pureharvest/df32"
)

var ErrTableNotFound = errors.New("table not found")
var ErrFilelistFull = errors.New("filelist full")

type Options struct {
	Driver              string
	MigrationsPath      string
	URL                 string
	OutputPath          string
	FileDefinitionsPath string
	IntermediatePath    string
	FilelistPath        string
	MigrationsTable     string
	ExtraOptions        map[string]string
}

type Driver interface {
	Initialize(cfg Options) error
	Bootstrap(url string) error
	Baseline(version uint64) error
	Apply(file Migration) error
	Version() (uint64, error)
	Model() (Model, error)
	GenerateFiles(Options, Model) error
	SetLogger(logger *slog.Logger)
	Close() error
}

type Logger interface {
	Printf(string, ...interface{})
	Println(...interface{})
}

func NewMemoryDriver() MemoryDriver {
	return MemoryDriver{
		make(map[uint64]bool),
		make([]*df32.Table, 4092),
	}
}

type MemoryDriver struct {
	versions map[uint64]bool
	Tables   []*df32.Table
}

func BuildModelFromMigrations(m []Migration) (Model, error) {

	tables := NewMemoryDriver()

	for _, migration := range m {
		if err := tables.Apply(migration); err != nil {
			return Model{}, err
		}
	}

	return tables.Model()
}

func (d *MemoryDriver) Version() (uint64, error) {
	var version uint64

	for v := range d.versions {
		if v > version {
			version = v
		}
	}

	return version, nil
}

func (d *MemoryDriver) Model() (Model, error) {

	version, _ := d.Version()

	m := NewModel(version)

	for i, table := range d.Tables {
		if table != nil {
			m.Set(i, *table)
		}
	}

	return m, nil
}

func (model *MemoryDriver) NextAvailableNumber() (int, error) {

	for i := range model.Tables {
		if model.Tables[i] == nil {
			return i, nil
		}
	}

	return 0, ErrFilelistFull
}

func (model *MemoryDriver) Get(number int) (df32.Table, error) {

	table := model.Tables[number]

	if table == nil {
		return df32.Table{}, ErrTableNotFound
	}

	return *table, nil
}

func (model *MemoryDriver) GetByName(name string) (df32.Table, error) {

	for _, table := range model.Tables {
		if table != nil && table.Name == name {
			return *table, nil
		}
	}

	return df32.Table{}, ErrTableNotFound
}

func (model *MemoryDriver) Set(number int, table df32.Table) error {

	model.Tables[number] = &table
	return nil
}

func (model *MemoryDriver) DeleteTable(number int) error {

	model.Tables[number] = nil

	return nil
}

func (model *MemoryDriver) Apply(migration Migration) error {

	if migration.Tables == nil {
		if migration.Direction == Up {
			model.versions[migration.Version] = true
		} else if migration.Direction == Down {
			delete(model.versions, migration.Version)
		}
		return nil
	}

	for _, tableMigration := range migration.Tables {
		var err error

		switch tableMigration.Action {
		case CreateAction:
			err = model.applyCreateMigration(tableMigration)
		case AlterAction:
			err = model.applyAlterMigration(tableMigration)
		case DeleteAction:
			err = model.applyDeleteMigration(tableMigration)
		default:
			err = errors.New("unknown action")
		}

		if err != nil {
			return err
		}
	}

	if migration.Direction == Up {
		model.versions[migration.Version] = true
	} else if migration.Direction == Down {
		delete(model.versions, migration.Version)
	}

	return nil
}

func (model *MemoryDriver) findTable(migration TableMigration) (table df32.Table, err error) {

	if migration.Number != 0 {
		table, err = model.Get(migration.Number)
	} else {
		table, err = model.GetByName(migration.Name)
	}

	return table, err
}

func (model *MemoryDriver) applyCreateMigration(migration TableMigration) error {

	if migration.Name == "" {
		return errors.New("create migrations must have a name")
	}

	migration = prepareCreateMigration(migration)

	for i := range migration.Fields {
		if strings.EqualFold(string(migration.Fields[i].Type), "char") {
			migration.Fields[i].Type = df32.AsciiType
		} else if strings.EqualFold(string(migration.Fields[i].Type), "datetime") {
			migration.Fields[i].Type = df32.DateType
		}

	}

	if migration.Number == 0 {
		next, err := model.NextAvailableNumber()

		if err != nil {
			return err
		}

		migration.Number = next
	} else {
		if _, err := model.Get(migration.Number); err != ErrTableNotFound {
			return errors.New("table with that name already exists")
		}
	}

	table := migration.Table()

	return model.Set(migration.Number, table)

}

func (model *MemoryDriver) applyAlterMigration(migration TableMigration) error {
	table, err := model.findTable(migration)

	if err != nil {
		return err
	}

	for i := range migration.Fields {
		if strings.EqualFold(string(migration.Fields[i].Type), "char") {
			migration.Fields[i].Type = df32.AsciiType
		} else if strings.EqualFold(string(migration.Fields[i].Type), "datetime") {
			migration.Fields[i].Type = df32.DateType
		}

	}

	table, err = AlterTable(table, migration)

	if err != nil {
		return err
	}

	return model.Set(int(table.Number), table)
}

func (model *MemoryDriver) applyDeleteMigration(migration TableMigration) error {
	table, err := model.findTable(migration)

	if err != nil {
		return err
	}

	return model.DeleteTable(int(table.Number))
}
