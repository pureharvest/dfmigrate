Dataflex Migration Tool
=======================

dfmigrate is a schema migration tool for dataflex programs backed by SQL Server database using the MSSQLDRV driver.

Migration File Spec
-------------------

A schema migration is specified in two (2) YAML file describing the changes to be made.

The filenames have the format **XXXXX_&lt;name&gt;.up.yml** and **XXXXX_&lt;name&gt;.down.yml**.

XXXXX is used for controlling the order the migrations will be applied. Up migrations will be applied from the lowest number to the highest number. Down migrations will be applied in the opposite order.

Schema migrations can take 3 forms:

Table Migrations
----------------

Table migrations can create, alter or delete tables.

To specify a table migration use the root ```tables``` key which is a list of table migrations.

Table Migration Fields:

- **action**: ```create || alter || delete```
- **name**: The name of the table. *Required
- **file_num**: The file number for the table. When creating tables if no file number is given the next available file number will be used.
- **display_name**: The Display Name for the table.
- **schema**: SQL Server schema name (defaults to ```dataflex```)
- **system**: Defines whether this is a system table (defaults to ```false```).
- **fields**: A list of field migrations.
- **indexes**: A list of index migrations.


Field Migrations:

- **action**: ```create || alter || delete```
- **name**: The name of the field
- **number**: The number of the field (currently not used).
- **type**: The data type. ```ascii || number || date || text```
- **size**: The size of the field. For ```number``` fields use the format &lt;length&gt;.&lt;precision&gt; e.g. ```10.0 || 10.4```
- **relates_to**: 
- **use_index**: The index numer to use for lookups.

Index Migrations:

- **action**:```create || delete```
- **name**: The name of the index. *Required
- **number**: The index number. (defaults to 1 more than the current number of indexes)
- **primary_key**: Is this the primary key for the table.
- **fields**: A list of fields that will be in the index. The order that they are specified is the order of the fields in the index.

Index Field Migration:

- **name**: The name of the field
- **descending**: Is the field descending.
- **uppercase**: Is the field Uppercased because comparing.

Example:
```
---
# Migration for EXAMPLE
tables:
- name: Example
  action: create
  file_num: 100
  display_name: Example Table for dataflex
  fields:
    - name: NUMBER
      type: ascii
      size: 10
      use_index: 1

    - name: SUPPLIER
      type: ascii
      size: 15
      use_index: 1

    - name: STOCK
      type: ascii
      size: 20

    - name: BARCODE
      type: ascii
      size: 20
      use_index: 1

    - name: FOUND
      type: ascii
      size: 1

    - name: DELETED
      type: date
      size: 6

  indexes:
    - name: EXAMPLE001_PK
      clustered: true
      fields:
        - name: BARCODE
          descending: NO
          uppercase: YES
        - name: NUMBER
          descending: NO
          uppercase: YES
        - name: SUPPLIER
          descending: NO
          uppercase: YES
```
SQL Migrations
----------------

SQL Migrations are raw sql that will be executed against SQL Server. This should only be used if the same action could not be accomplished via table migrations.

To specify a SQL migration use the root ```sql``` key. It should be a string literal.

Example:
```
sql: |
  DROP TABLE [dataflex].[TABLE001];
  DROP TABLE [dataflex].[TABLE002];
```

Program Migrations
----------------

Program Migrations are Dataflex programs that will be run.

To specify a Program migration use the root ```programs``` key. It is a list of programs that will be run in order.

The format of the program is what you would time at the command line without ```dfrun```.

The programs will be run from inside the migrations folder so all relative filenames should be relative to that folder.

Example:
```
programs:
  - dfimport c d file.csv 100
```