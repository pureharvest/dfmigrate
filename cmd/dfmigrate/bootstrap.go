// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"log"

	"github.com/spf13/cobra"
)

// bootstrapCmd represents the bootstrap command
var bootstrapCmd = &cobra.Command{
	Use:   "bootstrap",
	Short: "Bootstrap will create a new database and apply all migrations to it",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

		if err := migrator.Initialize(options); err != nil {
			log.Fatal(err)
		}

		if err := migrator.Bootstrap(); err != nil {
			log.Fatal(err)
		}

	},
}

func init() {
	RootCmd.AddCommand(bootstrapCmd)

}
