// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"github.com/spf13/cobra"
)

// resetCmd represents the reset command
var resetCmd = &cobra.Command{
	Use:   "reset",
	Short: "Reset the database.",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		// if err := migrator.Set(viper.GetString("url")); err != nil {
		// 	log.Fatal(err)
		// }
		// path := viper.GetString("path")

		// verifyMigrationsPath(path)

		// ctx := context.Background()

		// if err := migrator.Reset(ctx, path); err != nil {
		// 	log.Println(err)
		// 	os.Exit(1)
		// }

	},
}

func init() {
	RootCmd.AddCommand(resetCmd)

	// Here you will define your flags and configuration settings.

}
