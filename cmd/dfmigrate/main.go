package main

import (
	"fmt"
	"os"

	_ "gitlab.com/pureharvest/dfmigrate/mdspgsql"
	_ "gitlab.com/pureharvest/dfmigrate/mssqldrv"
)

func main() {

	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
