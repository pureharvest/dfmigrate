// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"log/slog"
	"os"
	"strings"

	"path/filepath"

	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/pureharvest/dfmigrate"
	"gitlab.com/pureharvest/dfmigrate/mdspgsql"
	"gitlab.com/pureharvest/dfmigrate/mssqldrv"
)

var version = "0.2.0-dev"

var (
	cfgFile  string
	driver   string
	path     string
	server   string
	database string
	username string
	password string
	sslmode  string
	logLevel string

	fdPath           string
	intermediatePath string
	filelistPath     string
	applyOnly        bool

	generateFileDefinitions bool
	generateHeaders         bool

	migrator dfmigrate.Migrator
	options  dfmigrate.Options
	logger   *slog.Logger
)

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "dfmigrate",
	Short: fmt.Sprintf("dfmigrate %s is a tool to migrate SQL backed Dataflex projects.", version),
	Long:  ``,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {

	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		migrator.Close()
	},
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags, which, if defined here,
	// will be global for your application.

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.dfmigrate/dfmigrate.yml)")
	RootCmd.PersistentFlags().StringVar(&driver, "driver", "mssqldrv", "database driver to use for migrations")
	RootCmd.PersistentFlags().StringVar(&path, "migrations-path", "", "path to migrations")
	RootCmd.PersistentFlags().StringVar(&server, "dsn", "", "connection string for the database")
	RootCmd.PersistentFlags().StringVarP(&fdPath, "file-definitions-path", "", "", "path to file definitions")
	RootCmd.PersistentFlags().StringVarP(&intermediatePath, "intermediates-path", "", "", "path to intermediate files")
	RootCmd.PersistentFlags().StringVarP(&filelistPath, "filelist", "", "", "path to filelist.cfg")
	RootCmd.PersistentFlags().StringVarP(&logLevel, "log-level", "", "error", "log level: error, warn, info, debug")

	if err := viper.BindPFlags(RootCmd.PersistentFlags()); err != nil {
		log.Fatal(err)
	}

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	dfpath := []string{}

	// Set the config search path
	if len(dfpath) != 0 {
		for _, p := range dfpath {
			viper.AddConfigPath(p)
		}
	}

	viper.SetConfigName("dfmigrate")                          // name of config file (without extension)
	viper.AddConfigPath(".")                                  // adding current directory as first search path
	viper.AddConfigPath(filepath.Join("$HOME", ".dfmigrate")) // adding home directory as second search path
	viper.SetEnvPrefix("DF")

	viper.AutomaticEnv() // read in environment variables that match

	if cfgFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(cfgFile)
	}

	viper.RegisterAlias("migrationsPath", "migrations-path")
	viper.RegisterAlias("fileDefinitionsPath", "file-definition-path")
	viper.RegisterAlias("intermediatesPath", "intermediate-path")
	viper.RegisterAlias("migrations_path", "migrations-path")
	viper.RegisterAlias("file_definitions_path", "file-definitions-path")
	viper.RegisterAlias("intermediates_path", "intermediates-path")

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		configFile := viper.ConfigFileUsed()
		fmt.Println("Using config file:", configFile)

		if err := setAbsPath("migrations_path"); err != nil {
			log.Fatalf("Error setting migrations path: %s", err)
		}

		if err := setAbsPath("filelist"); err != nil {
			log.Fatalf("Error setting filelist path: %s", err)
		}

		if err := setAbsPath("file_definitions_path"); err != nil {
			log.Fatalf("Error setting fd-path path: %s", err)
		}

		if err := setAbsPath("intermediates_path"); err != nil {
			log.Fatalf("Error setting intermediate-path path: %s", err)
		}

	}

	migrator.Register("mssqldrv", &mssqldrv.Driver{})
	migrator.Register("mdspgsql", &mdspgsql.Driver{})

	options = dfmigrate.Options{
		Driver:              viper.GetString("driver"),
		URL:                 viper.GetString("dsn"),
		OutputPath:          viper.GetString("output_path"),
		MigrationsPath:      viper.GetString("migrations_path"),
		FileDefinitionsPath: viper.GetString("file_definitions_path"),
		IntermediatePath:    viper.GetString("intermediates_path"),
		FilelistPath:        viper.GetString("filelist"),
	}

	level := slog.LevelError

	switch strings.ToLower(logLevel) {
	case "debug":
		level = slog.LevelDebug
	case "info":
		level = slog.LevelInfo
	case "warn":
		level = slog.LevelWarn
	}

	logger = slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		Level: level,
	}))

}

func verifyMigrationsPath(path string) {
	if path == "" {
		fmt.Println("Please specify path")
		os.Exit(1)
	}
}

func verifyFilelistPath(path string) {
	var err error

	configDir := filepath.Dir(viper.ConfigFileUsed())

	if !filepath.IsAbs(path) {
		path, err = filepath.Abs(filepath.Join(configDir, path))
	}

	if err != nil {
		log.Fatalf("Could not find filelist: %s (%s)", path, err)
	}

	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Fatalf("Could not find filelist: %s", path)
	}
}

func setAbsPath(key string) error {
	path := viper.GetString(key)

	if path == "" {
		return nil
	}

	absPath, err := getAbsolutePath(viper.ConfigFileUsed(), path)

	if err != nil {
		return err
	}

	viper.Set(key, absPath)

	return nil
}

func getAbsolutePath(configFilePath, path string) (string, error) {

	if filepath.IsAbs(path) {
		return path, nil
	}

	configDir := filepath.Dir(viper.ConfigFileUsed())

	return filepath.Abs(filepath.Join(configDir, path))

}
