// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

// gotoCmd represents the goto command
var gotoCmd = &cobra.Command{
	Use:   "goto <v>",
	Short: "Goto a specific version",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

		if err := migrator.Initialize(options); err != nil {
			log.Fatal(err)
		}

		toVersion := cmd.Flags().Arg(0)
		toVersionInt, err := strconv.Atoi(toVersion)
		if err != nil || toVersionInt < 0 {
			fmt.Println("Unable to parse param <v>.")
			os.Exit(1)
		}

		if err := migrator.Goto(uint64(toVersionInt)); err != nil {
			log.Fatal(err)
		}

	},
}

func init() {
	RootCmd.AddCommand(gotoCmd)

}
