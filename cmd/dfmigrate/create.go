// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

var upExample = []byte(`---
## Migration for EXAMPLE
## This is a example migration you can uncomment the following to use as a base
## for new migrations
# tables:
# - name: Example
#   action: create
#   file_num: 100
#   display_name: Example Table for dataflex
#   fields:
#     - name: ASCII_FIELD
#       type: ascii
#       size: 10
#       use_index: 1

#     - name: NUMBER_FIELD
#       type: number
#       size: 10.2

#     - name: DATE_FIELD
#       type: date

#   indexes:
#     - name: EXAMPLE001_PK
#       fields:
#         - name: ASCII_FIELD
#           descending: NO
#           uppercase: YES
#         - name: NUMBER_FIELD
#           descending: NO
#           uppercase: YES
#         - name: RECNUM
`)

var downExample = []byte(`---
## Down Migration for EXAMPLE
## Down migrations should perform the opposite action of the Up migration. The state should
## be left as it was before the up migration was applied.
# tables:
# - name: Example
#   action: delete
`)

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create <name>",
	Short: "Create a new migration",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

		if err := migrator.Initialize(options); err != nil {
			log.Fatal(err)
		}

		name := cmd.Flags().Arg(0)
		if name == "" {
			fmt.Println("Please specify name.")
			os.Exit(1)
		}

		migrationFile, err := migrator.Create(name)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		err = ioutil.WriteFile(filepath.Join(options.MigrationsPath, migrationFile.UpMigration.Filename()), upExample, 0644)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		err = ioutil.WriteFile(filepath.Join(options.MigrationsPath, migrationFile.DownMigration.Filename()), downExample, 0644)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		fmt.Printf("Version %v migration files created in %v:\n", migrationFile.Version, options.MigrationsPath)
		fmt.Println(migrationFile.UpMigration.Filename())
		fmt.Println(migrationFile.DownMigration.Filename())

	},
}

func init() {
	RootCmd.AddCommand(createCmd)

}
