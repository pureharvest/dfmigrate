// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
)

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Get the current version of the database",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		if err := migrator.Initialize(options); err != nil {
			log.Fatal(err)
		}
		version, err := migrator.Version()
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(version)

	},
}

func init() {
	RootCmd.AddCommand(versionCmd)

}
