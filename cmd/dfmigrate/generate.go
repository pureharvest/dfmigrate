// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

// generateCmd represents the up command
var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "generate all needed files",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		var err error

		if err := migrator.Initialize(options); err != nil {
			log.Fatal(err)
		}

		toVersionInt := uint64(0)
		toVersion := cmd.Flags().Arg(0)

		if toVersion == "" {
			nextVersion, err := migrator.GetNextVersion()
			if err != nil {
				log.Fatal("error getting last version: ", err)
			}

			toVersionInt = nextVersion - 1
		} else {
			toVersionInt, err = strconv.ParseUint(toVersion, 10, 64)
			if err != nil || toVersionInt < 0 {
				fmt.Println("Unable to parse param <v>.")
				os.Exit(1)
			}
		}

		if err := migrator.Generate(uint64(toVersionInt)); err != nil {
			log.Fatal("Couldn't generate files: ", err)
		}

	},
}

func init() {
	RootCmd.AddCommand(generateCmd)

}
