// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"log"
	"os"

	"github.com/spf13/cobra"
)

// downCmd represents the down command
var downCmd = &cobra.Command{
	Use:   "down",
	Short: "Run all down migrations",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		if err := migrator.Initialize(options); err != nil {
			log.Fatal(err)
		}

		if err := migrator.Down(); err != nil {
			log.Println(err)
			os.Exit(1)
		}

	},
}

func init() {
	RootCmd.AddCommand(downCmd)

}
