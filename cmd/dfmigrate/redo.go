// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"log"
	"os"

	"github.com/spf13/cobra"
)

// redoCmd represents the redo command
var redoCmd = &cobra.Command{
	Use:   "redo",
	Short: "Redo the last migration",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		if err := migrator.Initialize(options); err != nil {
			log.Fatal(err)
		}

		if err := migrator.Redo(); err != nil {
			log.Println(err)
			os.Exit(1)
		}

	},
}

func init() {
	RootCmd.AddCommand(redoCmd)

}
