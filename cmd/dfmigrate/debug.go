// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// debugCmd represents the debug command
var debugCmd = &cobra.Command{
	Use:   "debug",
	Short: "Print debug information",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		migrator.Initialize(options)

		fmt.Printf(`
Environment:
	Config File: %s
	Options: %#v
`,
			viper.ConfigFileUsed(), options)
	},
}

func init() {
	RootCmd.AddCommand(debugCmd)
}
