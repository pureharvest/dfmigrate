// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

// baselineCmd represents the baseline command
var baselineCmd = &cobra.Command{
	Use:   "baseline <n>",
	Short: "Set the version of the database",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		if err := migrator.Initialize(options); err != nil {
			log.Fatal(err)
		}
		version := cmd.Flags().Arg(0)
		versionInt, err := strconv.ParseUint(version, 10, 64)
		if err != nil {
			fmt.Println("Unable to parse param <n>.")
			os.Exit(1)
		}

		if err := migrator.Baseline(versionInt); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	},
}

func init() {
	RootCmd.AddCommand(baselineCmd)
}
