// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"log"

	"github.com/spf13/cobra"
)

// upCmd represents the up command
var upCmd = &cobra.Command{
	Use:   "up",
	Short: "Apply all up mirations",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		if err := migrator.Initialize(options); err != nil {
			log.Fatal(err)
		}

		if err := migrator.Up(); err != nil {
			log.Fatal(err)
		}

	},
}

func init() {
	RootCmd.AddCommand(upCmd)

}
