// Copyright © 2017 Pieter Lazzaro <pieter.lazzaro@pureharvest.com.au>

package main

import (
	"fmt"

	"gitlab.com/pureharvest/dfmigrate"

	"log"

	"github.com/spf13/cobra"
)

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List all available migrations.",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

		if err := migrator.Initialize(options); err != nil {
			log.Fatal(err)
		}

		version, err := migrator.Version()

		if err != nil {
			log.Fatal(err)
		}

		files, err := dfmigrate.ReadMigrationFiles(path)

		if err != nil {
			log.Fatal(err)
		}

		for _, file := range files {
			if version >= file.Version {
				fmt.Printf(" %d: %s\n", file.Version, file.UpMigration.Name)
			} else {
				fmt.Printf("* %d: %s\n", file.Version, file.UpMigration.Name)
			}

		}
	},
}

func init() {
	RootCmd.AddCommand(listCmd)

}
