package dfmigrate

import (
	"fmt"
	"io/ioutil"
	"log"
	"log/slog"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/bmizerany/assert"
	"github.com/spf13/viper"
	"gitlab.com/pureharvest/df32"
)

type TestEnvironment struct {
	Config        *viper.Viper
	Env           df32.Environment
	RootPath      string
	MigrationPath string
	ExpectedPath  string
	Cleanup       func()
	Migrator      Migrator
	Logger        *slog.Logger
}

func (env TestEnvironment) Validate(t *testing.T) {
	t.Log("Checking environment", env.ExpectedPath)
	err := filepath.Walk(env.ExpectedPath, func(path string, info os.FileInfo, err error) error {
		t.Logf("checking file %s", path)

		if path == env.ExpectedPath {
			return nil
		}

		if info.IsDir() {
			return filepath.SkipDir
		}

		srcFile, err := ioutil.ReadFile(path)

		if err != nil {
			return err
		}

		destFile, err := ioutil.ReadFile(filepath.Join(env.RootPath, filepath.Base(path)))

		if err != nil {
			return err
		}

		sData := strings.Split(string(srcFile), "\n")
		dData := strings.Split(string(destFile), "\n")

		for i, expected := range sData {
			actual := ""
			expected = strings.TrimRight(expected, "\r")

			if i < len(dData) {
				actual = strings.TrimRight(dData[i], "\r")
			}

			assert.Equal(t, expected, actual, fmt.Sprintf("Mismatch at line %d: %s vs %s", i, expected, actual))
		}

		return nil
	})

	t.Logf("finished checking enviornment")
	if err != nil {
		env.Cleanup()
		t.Fatal(err)
	}
}

func CreateTestEnvironment(t *testing.T, path string) TestEnvironment {

	// Create a temp directory to store changed files
	tmpDir, err := ioutil.TempDir(os.TempDir(), path)

	if err != nil {
		t.Fatal("could not create test environment")
	}

	cleanup := func() {

		if err := os.RemoveAll(tmpDir); err != nil {
			t.Fatal(err)
		}

	}

	testPath := filepath.Join(".", "test-fixtures", path)
	dataPath := filepath.Join(testPath, "data")

	env := TestEnvironment{
		RootPath:      tmpDir,
		MigrationPath: filepath.Join(testPath, "migrations"),
		ExpectedPath:  filepath.Join(testPath, "expected"),
		Logger: slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
			Level: slog.LevelDebug,
		})),
	}

	err = filepath.Walk(dataPath, func(path string, info os.FileInfo, err error) error {

		if path == dataPath {
			return nil
		}

		if info.IsDir() {
			return filepath.SkipDir
		}

		// could change this to io.Copy. All files are expected to be small.
		srcFile, err := ioutil.ReadFile(path)

		if err != nil {
			return err
		}

		err = ioutil.WriteFile(filepath.Join(env.RootPath, filepath.Base(path)), srcFile, 0600)

		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		cleanup()
		t.Fatal(err)
	}

	fl, err := df32.NewFilelist(filepath.Join(env.RootPath, "filelist.cfg"))

	if err != nil {
		cleanup()
		t.Fatal(err)
	}

	if err := fl.Save(); err != nil {
		cleanup()
		t.Fatal(err)
	}

	env.Env = df32.NewEnvironment(env.RootPath)

	env.Cleanup = func() {
		log.Println("cleaning up")
		env.Migrator.Close()
		cleanup()
	}

	env.Config = viper.New()
	env.Config.Set("migrations_path", env.MigrationPath)
	env.Config.Set("filelist_path", fl.Filename)
	env.Migrator = Migrator{
		Log: env.Logger,
	}

	return env
}
