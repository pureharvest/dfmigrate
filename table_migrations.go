package dfmigrate

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/pureharvest/df32"
)

type Action string

func (a *Action) UnmarshalJSON(data []byte) error {
	value := Action(strings.Trim(strings.ToLower(string(data)), `"`))

	if value == CreateAction || value == AlterAction || value == DeleteAction {
		*a = value
		return nil
	}

	return errors.New("invalid action")
}

var (
	CreateAction Action = "create"
	AlterAction  Action = "alter"
	DeleteAction Action = "delete"
)

type TableMigration struct {
	Action Action

	Number      int    `json:"file_num"`
	DisplayName string `json:"display_name"`
	Name        string
	System      bool

	Fields  []FieldMigration
	Indexes []IndexMigration
}

func (t TableMigration) String() string {
	return fmt.Sprintf("%s table %s with number %d", t.Action, t.Name, t.Number)
}

type FieldMigration struct {
	TableName string
	Action    Action
	Number    int
	Name      string
	Type      df32.Type
	Size      string
	Length    int
	Precision int
	Index     int `json:"use_index"`
	NewName   string
	RelatesTo df32.FieldRelation `json:"relates_to"`
}

func parseFieldSize(f string) (length int, precision int, err error) {

	parts := strings.Split(f, ".")

	length, err = strconv.Atoi(parts[0])

	if err != nil {
		return 0, 0, err
	}

	if len(parts) > 1 {

		precision, err = strconv.Atoi(parts[1])

		if err != nil {
			return 0, 0, err
		}
	}

	return length + precision, precision, nil
}

type fieldSize struct {
	IsSet     bool
	Length    int
	Precision int
}

func (f fieldSize) String() string {
	if f.IsSet {
		if f.Precision != 0 {
			return fmt.Sprintf("%d.%d", f.Length-f.Precision, f.Precision)
		}

		return fmt.Sprintf("%d", f.Length)
	}

	return ""
}

func (f *fieldSize) UnmarshalJSON(data []byte) error {
	length, precision, err := parseFieldSize(strings.Trim(string(data), `"'`))

	if err != nil {
		return err
	}

	f.Length = length
	f.Precision = precision
	f.IsSet = true

	return nil
}

func (a *FieldMigration) UnmarshalJSON(data []byte) error {
	field := struct {
		TableName string
		Action    Action
		Number    int
		Name      string
		Type      df32.Type
		Size      fieldSize
		Length    int
		Precision int
		Index     int `json:"use_index"`
		NewName   string
		RelatesTo df32.FieldRelation `json:"relates_to"`
	}{}

	if err := json.Unmarshal(data, &field); err != nil {
		return err
	}

	if field.Size.IsSet && field.Length != 0 {
		return errors.New("cannot set field size and length. choose one or the other")
	}

	field.Length = field.Size.Length
	field.Precision = field.Size.Precision

	a.TableName = field.TableName
	a.Action = field.Action
	a.Number = field.Number
	a.Name = field.Name
	a.Type = field.Type
	a.Size = field.Size.String()
	a.Length = field.Length
	a.Precision = field.Precision
	a.Index = field.Index
	a.NewName = field.NewName
	a.RelatesTo = field.RelatesTo

	return nil
}

func (field FieldMigration) Field() df32.Field {
	if field.Type == df32.DateType {
		field.Length = 6
		field.Precision = 0
	}

	return df32.Field{
		Number:    field.Number,
		Name:      field.Name,
		Type:      field.Type,
		Length:    field.Length,
		Precision: field.Precision,
		RelatesTo: field.RelatesTo,
		Index:     field.Index,
	}
}

type IndexMigration struct {
	Action     Action
	Number     int
	Name       string
	PrimaryKey bool `json:"primary_key"`
	Fields     []IndexField
}

func (index IndexMigration) Index() df32.Index {
	dfIndex := df32.Index{
		Name:       index.Name,
		Number:     index.Number,
		PrimaryKey: index.PrimaryKey,
		Fields:     make([]df32.IndexField, 0, len(index.Fields)),
	}

	for _, field := range index.Fields {
		dfIndex.Fields = append(dfIndex.Fields, field.IndexField())
	}

	return dfIndex
}

type IndexField struct {
	Number     int
	Name       string
	Uppercase  bool
	Descending bool
}

func (field IndexField) IndexField() df32.IndexField {
	return df32.IndexField{
		Number:     field.Number,
		Name:       field.Name,
		Uppercase:  field.Uppercase,
		Descending: field.Descending,
	}
}

func (m TableMigration) Table() df32.Table {

	fields := make([]df32.Field, 0, len(m.Fields)+1)
	indexes := make([]df32.Index, 0, len(m.Indexes))

	for i, f := range m.Fields {
		field := f.Field()
		field.Number = i + 1
		fields = append(fields, field)
	}

	for _, index := range m.Indexes {
		indexes = append(indexes, index.Index())
	}

	return df32.Table{
		Number:      int64(m.Number),
		Name:        m.Name,
		DisplayName: m.DisplayName,
		System:      m.System,
		Fields:      fields,
		Indexes:     indexes,
	}
}

func prepareCreateMigration(migration TableMigration) TableMigration {
	fields := map[string]FieldMigration{}

	for i, field := range migration.Fields {
		if field.Number == 0 {
			field.Number = i + 1
		}

		fields[field.Name] = field

		migration.Fields[i] = field
	}

	for i, index := range migration.Indexes {
		index.Number = i + 1
		for j, field := range index.Fields {
			if field.Number == 0 {
				index.Fields[j].Number = fields[field.Name].Number

			}
		}

		migration.Indexes[i] = index
	}

	return migration
}

func prepareAlterMigration(table df32.Table, migration TableMigration) (TableMigration, error) {

	fields := map[string]FieldMigration{}

	for i, index := range migration.Indexes {
		if index.Number == 0 {
			index.Number = len(table.Indexes) + i
		}

		for j, field := range index.Fields {
			if field.Number == 0 {
				index.Fields[j].Number = fields[field.Name].Number

			}
		}
		migration.Indexes[i] = index
	}

	return migration, nil
}

func PrepareTableMigration(migration TableMigration) (TableMigration, error) {

	if migration.Action == CreateAction {
		return prepareCreateMigration(migration), nil
	}

	return migration, nil
}

func ApplyMigration(table df32.Table, migration TableMigration) (df32.Table, error) {
	if migration.Action == CreateAction {
		newTable := migration.Table()

		if table.Number != 0 {
			newTable.Number = table.Number
		}

		if table.Name != "" {
			newTable.Name = table.Name
		}

		return newTable, nil
	}

	return AlterTable(table, migration)
}

// findField returns the index of field in the fields or -1 if not found
func findField(fields []df32.Field, name string) int {
	for i, f := range fields {
		if f.Name == name {
			return i
		}
	}

	return -1
}

// findIndex returns the index of index in the indexes or -1 if not found
func findIndex(indexes []df32.Index, name string) int {
	for i, ind := range indexes {
		if ind.Name == name {
			return i
		}
	}

	return -1
}

func modifyField(from df32.Field, fieldMigration FieldMigration) df32.Field {

	if fieldMigration.NewName != "" {
		from.Name = fieldMigration.NewName
	}

	if fieldMigration.Type != "" {
		from.Type = fieldMigration.Type
	}

	if fieldMigration.Length != 0 {
		from.Length = fieldMigration.Length
	}

	if fieldMigration.Precision != 0 {
		from.Precision = fieldMigration.Precision
	}

	if fieldMigration.Index != 0 {
		from.Index = fieldMigration.Index
	}

	if fieldMigration.RelatesTo.Field != 0 {
		from.RelatesTo.Field = fieldMigration.RelatesTo.Field
	}

	if fieldMigration.RelatesTo.File != 0 {
		from.RelatesTo.File = fieldMigration.RelatesTo.File
	}

	return from
}

func AlterTable(table df32.Table, migration TableMigration) (df32.Table, error) {

	migration, err := prepareAlterMigration(table, migration)
	if err != nil {
		return table, err
	}

	for _, field := range migration.Fields {

		switch field.Action {
		case CreateAction:
			field.Number = len(table.Fields) + 1
			table.Fields = append(table.Fields, df32.Field{
				Number:    field.Number,
				Name:      field.Name,
				Type:      field.Type,
				Length:    field.Length,
				Precision: field.Precision,
				RelatesTo: field.RelatesTo,
				Index:     field.Index,
			})
		case AlterAction:
			i := findField(table.Fields, field.Name)

			if i == -1 {
				return table, errors.Errorf("could not find field %s", field.Name)
			}

			table.Fields[i] = modifyField(table.Fields[i], field)

		case DeleteAction:
			i := findField(table.Fields, field.Name)

			if i == -1 {
				return table, errors.Errorf("could not find field %s", field.Name)
			}

			table.Fields = append(table.Fields[:i], table.Fields[i+1:]...)
		}
	}

	for _, indexMigration := range migration.Indexes {
		switch indexMigration.Action {
		case CreateAction:
			index := df32.Index{
				Name: indexMigration.Name,
			}

			if index.Number == 0 {
				index.Number = len(table.Indexes)
			}

			for i, indexField := range indexMigration.Fields {
				index.Fields = append(index.Fields, df32.IndexField{
					Number:     i,
					Name:       indexField.Name,
					Uppercase:  indexField.Uppercase,
					Descending: indexField.Descending,
				})
			}

			table.Indexes = append(table.Indexes, index)

		case DeleteAction:
			i := findIndex(table.Indexes, indexMigration.Name)

			if i == -1 {
				return table, errors.Errorf("could not find index %s", indexMigration.Name)
			}

			table.Indexes = append(table.Indexes[:i], table.Indexes[i+1:]...)
		}
	}

	return table, nil
}
