FROM golang:1.23

RUN mkdir -p /src/
WORKDIR /src
COPY . /src

RUN go install ./cmd/dfmigrate

CMD ["dfmigrate"]
