package dfmigrate

import (
	"fmt"
	"io/ioutil"
	"path"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"

	"github.com/ghodss/yaml"
	"github.com/pkg/errors"
)

var filenameRegex = regexp.MustCompile(`^([0-9]+)_(.*)\.(up|down)\.yml$`)

type Direction int

const (
	Up   Direction = +1
	Down           = -1
)

func (d Direction) String() string {
	if d == Up {
		return "up"
	}

	if d == Down {
		return "down"
	}

	return "unkown"
}

// File represents one file on disk.
// Example: 001_initial_plan_to_do_sth.up.sql
type File struct {
	// absolute path to file
	Path string

	// the name of the file
	FileName string

	// version parsed from filename
	Version uint64

	// the actual migration name parsed from filename
	Name string

	// content of the file
	Content []byte

	// UP or DOWN migration
	Direction Direction
}

// Files is a slice of Files
type Files []File

// MigrationFile represents both the UP and the DOWN migration file.
type MigrationFile struct {
	// version of the migration file, parsed from the filenames
	Version uint64

	// reference to the *up* migration file
	UpMigration *Migration

	// reference to the *down* migration file
	DownMigration *Migration
}

// MigrationFiles is a slice of MigrationFiles
type MigrationFiles []MigrationFile

// ReadContent reads the file's content if the content is empty
func (f *File) ReadContent() error {
	if len(f.Content) == 0 {
		content, err := ioutil.ReadFile(path.Join(f.Path, f.FileName))
		if err != nil {
			return err
		}
		f.Content = content
	}
	return nil
}

// ToFirstFrom fetches all (down) migrations including the migration
// of the current version to the very first migration.
func (mf *MigrationFiles) ToFirstFrom(version uint64) ([]Migration, error) {
	sort.Sort(sort.Reverse(mf))
	migrations := make([]Migration, 0)
	for _, migrationFile := range *mf {
		if migrationFile.Version <= version && migrationFile.DownMigration != nil {
			migrations = append(migrations, *migrationFile.DownMigration)
		}
	}
	return migrations, nil
}

// ToLastFrom fetches all (up) migrations to the most recent migration.
// The migration of the current version is not included.
func (mf *MigrationFiles) ToLastFrom(version uint64) ([]Migration, error) {
	sort.Sort(mf)
	migrations := make([]Migration, 0)
	for _, migrationFile := range *mf {
		if migrationFile.Version > version && migrationFile.UpMigration != nil {
			migrations = append(migrations, *migrationFile.UpMigration)
		}
	}
	return migrations, nil
}

// From returns all migrations between two numbers inclusive.
func (mf *MigrationFiles) From(from uint64, to uint64) ([]Migration, error) {
	var d Direction

	relativeN := int64(to) - int64(from)

	if relativeN > 0 {
		d = Up
	} else if relativeN < 0 {
		d = Down
	} else { // relativeN == 0
		return nil, nil
	}

	if d == Down {
		sort.Sort(sort.Reverse(mf))
	} else {
		sort.Sort(mf)
	}

	migrations := make([]Migration, 0)

	for _, migrationFile := range *mf {
		if d == Up && migrationFile.UpMigration != nil && migrationFile.Version > from && migrationFile.Version <= to {
			migrations = append(migrations, *migrationFile.UpMigration)
		} else if d == Down && migrationFile.DownMigration != nil && migrationFile.Version <= from && migrationFile.Version > to {
			migrations = append(migrations, *migrationFile.DownMigration)
		}
	}
	return migrations, nil
}

// Len is the number of elements in the collection.
// Required by Sort Interface{}
func (mf MigrationFiles) Len() int {
	return len(mf)
}

// Less reports whether the element with
// index i should sort before the element with index j.
// Required by Sort Interface{}
func (mf MigrationFiles) Less(i, j int) bool {
	return mf[i].Version < mf[j].Version
}

// Swap swaps the elements with indexes i and j.
// Required by Sort Interface{}
func (mf MigrationFiles) Swap(i, j int) {
	mf[i], mf[j] = mf[j], mf[i]
}

// ReadMigrationFiles reads all migration files from a given path
func ReadMigrationFiles(path string) (files MigrationFiles, err error) {
	// find all migration files in path
	ioFiles, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, errors.Wrapf(err, "could not read migration files at %s. (%s)", path, err.Error())
	}
	type tmpFile struct {
		version  uint64
		name     string
		filename string
		d        Direction
	}
	tmpFiles := make([]*tmpFile, 0)
	tmpFileMap := map[uint64]map[Direction]tmpFile{}
	for _, file := range ioFiles {
		version, name, d, err := parseFilenameSchema(file.Name(), filenameRegex)
		if err == nil {
			if _, ok := tmpFileMap[version]; !ok {
				tmpFileMap[version] = map[Direction]tmpFile{}
			}

			if existing, ok := tmpFileMap[version][d]; !ok {
				tmpFileMap[version][d] = tmpFile{version: version, name: name, filename: file.Name(), d: d}
			} else {
				return nil, fmt.Errorf("duplicate migration file version %d : %q and %q", version, existing.filename, file.Name())
			}
			tmpFiles = append(tmpFiles, &tmpFile{version, name, file.Name(), d})
		}
	}

	newFiles := make(MigrationFiles, 0)

	for version, files := range tmpFileMap {
		migrationFile := MigrationFile{
			Version: version,
		}

		for d, file := range files {

			f, err := ioutil.ReadFile(filepath.Join(path, file.filename))

			if err != nil {
				return nil, fmt.Errorf("could not read file: %s", err.Error())
			}

			migration := Migration{
				Name:      file.name,
				Direction: file.d,
				Version:   file.version,
			}

			if err := yaml.Unmarshal(f, &migration); err != nil {
				return nil, err
			}

			if d == Up {
				migrationFile.UpMigration = &migration
			} else if d == Down {
				migrationFile.DownMigration = &migration
			}
		}
		newFiles = append(newFiles, migrationFile)
	}

	sort.Sort(newFiles)
	return newFiles, nil
}

// parseFilenameSchema parses the filename
func parseFilenameSchema(filename string, filenameRegex *regexp.Regexp) (version uint64, name string, d Direction, err error) {
	matches := filenameRegex.FindStringSubmatch(filename)
	if len(matches) != 4 && len(matches) != 5 {
		return 0, "", 0, errors.New("Unable to parse filename schema")
	}

	version, err = strconv.ParseUint(matches[1], 10, 0)
	if err != nil {
		return 0, "", 0, errors.New(fmt.Sprintf("Unable to parse version '%v' in filename schema", matches[0]))
	}

	if matches[3] == "up" {
		d = Up
	} else if matches[3] == "down" {
		d = Down
	} else {
		return 0, "", 0, errors.New(fmt.Sprintf("Unable to parse up|down '%v' in filename schema", matches[3]))
	}

	return version, matches[2], d, nil
}
