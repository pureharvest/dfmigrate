package dfmigrate

import (
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ReadFiles(t *testing.T) {
	path := filepath.Join(".", "test-fixtures", "01")

	files, err := ReadMigrationFiles(path)

	if err != nil {
		t.Fatal(err)
	}

	expected := MigrationFiles{
		MigrationFile{
			Version: 1,
			UpMigration: &Migration{
				Name:      "test",
				Direction: Up,
				Version:   1,
			},
			DownMigration: &Migration{
				Name:      "test",
				Direction: Down,
				Version:   1,
			},
		},
	}

	assert.Equal(t, expected, files)
}
