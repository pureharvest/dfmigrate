package mdspgsql

import (
	"testing"

	"gitlab.com/pureharvest/df32"

	"github.com/stretchr/testify/assert"
)

func Test_MDSPGSQLGeneratorCreateIndex(t *testing.T) {
	tests := []struct {
		name      string
		tableName string
		schema    string
		index     df32.Index
		expected  string
	}{
		{
			name:      "converts names to uppercase",
			tableName: "test",
			schema:    "test",
			index:     df32.Index{Name: "TEST01", Fields: []df32.IndexField{{Name: "field1", Descending: true}}},
			expected:  `CREATE UNIQUE INDEX "TEST01" ON "test"."TEST" ("FIELD1" DESC)`,
		},
		{
			name:      "ignores clustered property",
			tableName: "TEST",
			schema:    "test",
			index:     df32.Index{Name: "TEST01", Fields: []df32.IndexField{{Name: "FIELD1", Descending: true}}},
			expected:  `CREATE UNIQUE INDEX "TEST01" ON "test"."TEST" ("FIELD1" DESC)`,
		},
	}

	for _, test := range tests {
		t.Run(test.index.Name, func(t *testing.T) {
			g := MDSPGSQLGenerator{}
			sql := g.CreateIndex(test.schema, test.tableName, test.index.Name, test.index.Fields)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_MDSPGSQLGeneratorFieldDefintion(t *testing.T) {
	generator := defaultMDSPGSQLGenerator

	tests := []struct {
		name        string
		table       string
		field       df32.Field
		ignoresCase bool
		expected    string
	}{
		{
			name:        "converts to uppercase",
			table:       "TEST",
			field:       df32.Field{Name: "test", Type: df32.AsciiType, Length: 10},
			ignoresCase: false,
			expected:    `"TEST" VARCHAR(10) NOT NULL DEFAULT ''`,
		},
		{
			name:        "ascii",
			table:       "TEST",
			field:       df32.Field{Name: "TEST", Type: df32.AsciiType, Length: 10},
			ignoresCase: false,
			expected:    `"TEST" VARCHAR(10) NOT NULL DEFAULT ''`,
		},
		{
			name:        "date",
			table:       "TEST",
			field:       df32.Field{Name: "TEST", Type: df32.DateType},
			ignoresCase: false,
			expected:    `"TEST" DATE NOT NULL DEFAULT '0001-01-01'`,
		},
		{
			name:        "number without precision",
			table:       "TEST",
			field:       df32.Field{Name: "TEST", Type: df32.NumberType, Length: 10},
			ignoresCase: false,
			expected:    `"TEST" NUMERIC(10) NOT NULL DEFAULT 0`,
		},
		{
			name:        "number with precision",
			table:       "TEST",
			field:       df32.Field{Name: "TEST", Type: df32.NumberType, Length: 14, Precision: 4},
			ignoresCase: false,
			expected:    `"TEST" NUMERIC(14,4) NOT NULL DEFAULT 0`,
		},
	}

	for _, test := range tests {
		t.Run(test.table+test.field.Name, func(t *testing.T) {
			sql := generator.fieldDefinition(test.table, test.field, test.ignoresCase)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_MDSPGSQLGeneratorCreateTable(t *testing.T) {
	generator := MDSPGSQLGenerator{
		schema: "test",
	}

	tests := []struct {
		name     string
		table    df32.Table
		expected string
	}{
		{
			name: "no recnum, no primary key",
			table: df32.Table{
				Name: "TEST",
				Fields: []df32.Field{
					df32.Field{Name: "FIELD1", Type: df32.AsciiType, Length: 10},
					df32.Field{Name: "FIELD2", Type: df32.DateType, Length: 6},
				},
			},
			expected: `CREATE TABLE "test"."TEST" (
    "RECNUM" SERIAL,
    "FIELD1" VARCHAR(10) NOT NULL DEFAULT '',
    "FIELD2" DATE NOT NULL DEFAULT '0001-01-01',
    CONSTRAINT "TEST_INDEX00" PRIMARY KEY ("RECNUM")
) WITH (
    OIDS = TRUE
)`,
		},
		{
			name: "recnum, with primary key",
			table: df32.Table{
				Name: "TEST",
				Fields: []df32.Field{
					df32.Field{Name: "RECNUM", Type: df32.NumberType},
					df32.Field{Name: "FIELD1", Type: df32.AsciiType, Length: 10},
				},
				Indexes: []df32.Index{
					df32.Index{Name: "TEST001_PK", PrimaryKey: true, Fields: []df32.IndexField{{Name: "FIELD1", Descending: true}}},
				},
			},
			expected: `CREATE TABLE "test"."TEST" (
    "RECNUM" SERIAL,
    "FIELD1" VARCHAR(10) NOT NULL DEFAULT '',
    CONSTRAINT "TEST_INDEX00" UNIQUE ("RECNUM"),
    CONSTRAINT "TEST001_PK" PRIMARY KEY ("FIELD1")
) WITH (
    OIDS = TRUE
)`,
		},
		{
			name: "recnum, with primary key clustered",
			table: df32.Table{
				Name: "TEST",
				Fields: []df32.Field{
					df32.Field{Name: "RECNUM", Type: df32.NumberType},
					df32.Field{Name: "FIELD1", Type: df32.AsciiType, Length: 10},
				},
				Indexes: []df32.Index{
					df32.Index{Name: "TEST001_PK", PrimaryKey: true, Fields: []df32.IndexField{{Name: "FIELD1", Descending: true}}},
				},
			},
			expected: `CREATE TABLE "test"."TEST" (
    "RECNUM" SERIAL,
    "FIELD1" VARCHAR(10) NOT NULL DEFAULT '',
    CONSTRAINT "TEST_INDEX00" UNIQUE ("RECNUM"),
    CONSTRAINT "TEST001_PK" PRIMARY KEY ("FIELD1")
) WITH (
    OIDS = TRUE
)`,
		},
		{
			name: "system",
			table: df32.Table{
				System: true,
				Name:   "TEST",
				Fields: []df32.Field{
					df32.Field{Name: "FIELD1", Type: df32.AsciiType, Length: 10},
				},
			},
			expected: `CREATE TABLE "test"."TEST" (
    "RECNUM" SERIAL,
    "FIELD1" VARCHAR(10) NOT NULL DEFAULT '',
    CONSTRAINT "TEST_INDEX00" PRIMARY KEY ("RECNUM")
) WITH (
    OIDS = TRUE
)`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql := generator.CreateTable(test.table)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_MDSPGSQLGeneratorAlterField(t *testing.T) {
	generator := defaultMDSPGSQLGenerator

	tests := []struct {
		tableName   string
		schema      string
		from        string
		to          df32.Field
		ignoresCase bool
		expected    string
	}{
		{
			tableName:   "TEST",
			schema:      "test",
			from:        "FROM",
			to:          df32.Field{Type: df32.NumberType, Length: 14, Precision: 4},
			ignoresCase: false,
			expected:    `ALTER TABLE "test"."TEST" ALTER COLUMN "FROM" NUMERIC(14,4)`,
		},
	}

	for _, test := range tests {
		t.Run(test.tableName, func(t *testing.T) {
			sql := generator.AlterField(test.schema, test.tableName, test.from, test.to, test.ignoresCase)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_MDSPGSQLGeneratorRenameField(t *testing.T) {
	generator := defaultMDSPGSQLGenerator

	tests := []struct {
		tableName string
		schema    string
		from      string
		to        string
		expected  string
	}{
		{
			tableName: "TEST",
			schema:    "test",
			from:      "FROM",
			to:        "TO",
			expected:  `ALTER TABLE "test"."TEST" RENAME COLUMN "FROM" TO "TO"`,
		},
	}

	for _, test := range tests {
		t.Run(test.tableName, func(t *testing.T) {
			sql := generator.RenameField(test.schema, test.tableName, test.from, test.to)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_MDSPGSQLGeneratorAddField(t *testing.T) {
	generator := defaultMDSPGSQLGenerator

	tests := []struct {
		tableName   string
		schema      string
		field       df32.Field
		ignoresCase bool
		expected    string
	}{
		{
			tableName:   "TEST",
			schema:      "test",
			field:       df32.Field{Name: "FIELD1", Type: df32.AsciiType, Length: 10},
			ignoresCase: false,
			expected:    `ALTER TABLE "test"."TEST" ADD "FIELD1" VARCHAR(10) NOT NULL DEFAULT ''`,
		},
	}

	for _, test := range tests {
		t.Run(test.tableName, func(t *testing.T) {
			sql := generator.AddField(test.schema, test.tableName, test.field, test.ignoresCase)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_MDSPGSQLGeneratorDropField(t *testing.T) {
	generator := defaultMDSPGSQLGenerator

	tests := []struct {
		tableName string
		schema    string
		field     string
		expected  string
	}{
		{
			tableName: "TEST",
			schema:    "test",
			field:     "FIELD1",
			expected:  `ALTER TABLE "test"."TEST" DROP COLUMN "FIELD1"`,
		},
	}

	for _, test := range tests {
		t.Run(test.tableName, func(t *testing.T) {
			sql := generator.DropField(test.schema, test.tableName, test.field)

			assert.Equal(t, test.expected, sql)
		})
	}
}
