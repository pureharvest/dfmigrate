package mdspgsql

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"log/slog"
	neturl "net/url"
	"path/filepath"

	"gitlab.com/pureharvest/df32"
	"gitlab.com/pureharvest/dfmigrate"

	"strings"

	"os"

	"github.com/pkg/errors"

	_ "github.com/lib/pq"
)

type versionInfo uint64

const tableName = "schema_migrations"
const driverName = "mdspgsql"

type Driver struct {
	defaultConnectionString string

	intPath      string
	filelistPath string

	filelist df32.Filelist

	generator MDSPGSQLGenerator

	db  *sql.DB
	log *slog.Logger
}

type fileOp struct {
	operation string
	filename  string
	data      []byte
}

func (driver *Driver) Bootstrap(url string) error {
	parsedURL, err := neturl.Parse("postgres://" + url)

	if err != nil {
		return err
	}

	connectionString := fmt.Sprintf("%s?%s", parsedURL.Host, parsedURL.Query().Encode())
	database := parsedURL.Path

	db, err := sql.Open("postgres", "postgres://"+connectionString)

	if err != nil {
		return err
	}

	if err := db.Ping(); err != nil {
		return err
	}

	if _, err := db.Exec(fmt.Sprintf(`CREATE DATABASE "%s"`, database)); err != nil {
		return err
	}

	return db.Close()
}

// Driver will connect with mdspgsql://user:pass

func (driver *Driver) Initialize(cfg dfmigrate.Options) error {
	var err error

	driver.generator = defaultMDSPGSQLGenerator

	connectionString := "postgres://" + cfg.URL

	driver.intPath = cfg.IntermediatePath

	driver.filelistPath = cfg.FilelistPath

	if driver.filelistPath != "" {

		driver.filelist, err = df32.Open(driver.filelistPath)

		if err != nil {
			return errors.Wrap(err, "could not read filelist")
		}
	} else {
		driver.filelistPath = "filelist.cfg"
		driver.filelist, _ = df32.NewFilelist(driver.filelistPath)
	}

	if driver.db != nil {
		driver.db.Close()
	}

	driver.db, err = sql.Open("postgres", connectionString)

	if err != nil {
		return errors.Wrap(err, "could not connect to database")
	}

	if err := driver.db.Ping(); err != nil {
		return errors.Wrap(err, "could not connect to database")
	}

	if err := ensureSchemaExists(driver.db, driver.generator.schema); err != nil {
		return err
	}

	if err := driver.ensureVersionTableExists(); err != nil {
		return err
	}

	return nil

}

func ensureSchemaExists(db *sql.DB, schema string) error {
	if _, err := db.Exec(fmt.Sprintf(`
CREATE SCHEMA IF NOT EXISTS "%s";
`, schema)); err != nil {
		return err
	}
	return nil
}

func (driver *Driver) ensureVersionTableExists() error {
	if _, err := driver.db.Exec("CREATE TABLE IF NOT EXISTS " + tableName + " (version int not null primary key);"); err != nil {
		return err
	}
	return nil
}

func (driver *Driver) SetLogger(logger *slog.Logger) {
	driver.log = logger
}

func (driver *Driver) Close() error {

	return driver.db.Close()
}

func (driver *Driver) GenerateFiles(options dfmigrate.Options, model dfmigrate.Model) error {
	return nil
}

func (driver *Driver) Model() (dfmigrate.Model, error) {
	return driver.loadModel(driver.db)
}

func (driver *Driver) loadModel(db queryer) (dfmigrate.Model, error) {
	var err error
	var version uint64

	if version, err = driver.version(db); err != nil {
		return dfmigrate.Model{}, err
	}

	model := dfmigrate.NewModel(version)

	var intPath string

	for _, entry := range driver.filelist.GetAll() {
		if strings.EqualFold(entry.Driver, "DATAFLEX") {
			driver.log.Debug(fmt.Sprintf("%d:%s is a native table. ignoring...", entry.Number, entry.Name))
			continue
		}

		driver.log.Debug(fmt.Sprintf("loading table %s", entry.Name))

		intPath = filepath.Join(driver.intPath, strings.ToLower(entry.Filename()))

		intFile, err := os.Open(intPath)
		if err != nil {
			return model, err
		}
		defer intFile.Close()

		intermediateFile, err := loadIntermediateFile(intFile)

		if err != nil {
			return model, errors.Wrap(err, "could not read intermedaite file")
		}

		table, err := loadTableFromDB(intermediateFile, db)

		if err != nil {
			return model, errors.Wrapf(err, "could not load table: %s", entry.Name)
		}

		table.Number = entry.Number
		table.DisplayName = entry.DisplayName

		model.Set(int(entry.Number), table)
	}

	return model, nil
}

func (driver *Driver) Apply(file dfmigrate.Migration) error {
	driver.log.Debug(fmt.Sprintln("Running migration", file.Name, file.Direction))

	tx, err := driver.db.Begin()

	if err != nil {
		return errors.Wrap(err, "could not run migration")
	}

	if file.Direction == dfmigrate.Up {
		if _, err := tx.Exec(`INSERT INTO "`+tableName+`" (version) VALUES ($1)`, file.Version); err != nil {
			return driver.rollBackOnError(errors.Wrap(err, "insert"), tx)
		}
	} else if file.Direction == dfmigrate.Down {
		if _, err := tx.Exec(`DELETE FROM "`+tableName+`" WHERE version=$1`, file.Version); err != nil {
			return driver.rollBackOnError(errors.Wrap(err, "delete"), tx)
		}
	}

	var statements []string

	if len(file.Tables) > 0 {
		driver.log.Debug(fmt.Sprintln("load the current model"))

		model, err := driver.loadModel(tx)

		if err != nil {
			return errors.Wrap(err, "could not load the model from database")
		}

		// Prepare migrations
		driver.log.Debug(fmt.Sprintln("prepare dfmigrate..."))
		preparedMigrations := make([]dfmigrate.TableMigration, len(file.Tables))

		for i, migration := range file.Tables {
			driver.log.Debug(fmt.Sprintf("preparing %s...", migration.Name))

			migration, err := dfmigrate.PrepareTableMigration(migration)

			if err != nil {
				return driver.rollBackOnError(errors.Wrap(err, "unable to prepare migrations"), tx)
			}

			preparedMigrations[i] = migration
		}

		for _, migration := range file.Tables {
			driver.log.Debug(fmt.Sprintf("building sql for %s", migration.Name))

			s, err := GenerateTableMigrationScript(model, migration, driver.generator)

			if err != nil {
				return driver.rollBackOnError(err, tx)
			}

			statements = append(statements, s...)

		}

		driver.log.Debug(fmt.Sprintln("running sql..."))

		for _, statement := range statements {
			driver.log.Debug(fmt.Sprintln(statement))
			if _, err := tx.Exec(statement); err != nil {

				return driver.rollBackOnError(err, tx)
			}

		}

		driver.log.Debug(fmt.Sprintln("generating Dataflex files..."))

		if err := driver.generateFiles(preparedMigrations, model); err != nil {
			return driver.rollBackOnError(err, tx)
		}

	} else if len(file.SQL) != 0 {

		for _, statement := range file.SQL {
			driver.log.Debug(fmt.Sprintln(statement))
			_, err = tx.Exec(statement)

			if err != nil {
				return driver.rollBackOnError(err, tx)
			}
		}

	} else if len(file.Programs) != 0 {

		// for _, program := range migration.Programs {

		// 	log.Println("Running ", program)
		// 	cmd := driver.env.Command(program)

		// 	cmd.Dir = f.Path

		// 	cmd.Stdout = os.Stdout
		// 	cmd.Stderr = os.Stderr
		// 	cmd.Stdin = os.Stdin

		// 	err := cmd.Run()

		// 	if err != nil {
		// 		return driver.rollBackOnError(err, tx)
		// 	}

		// }

	}

	if err := tx.Commit(); err != nil {
		return driver.rollBackOnError(err, tx)
	}

	driver.log.Debug(fmt.Sprintln("Finished migration..."))
	return driver.filelist.Save()
}

func getTargetFilePath(driver *Driver, op fileOp) string {

	var prefix string

	if strings.Contains(op.filename, ".int") && driver.intPath != "" {
		prefix = driver.intPath
	} else {
		prefix = "."
	}

	if op.operation == "add" {
		return filepath.Join(prefix, op.filename)
	}

	return ""

}

func (driver *Driver) generateFiles(preparedMigrations []dfmigrate.TableMigration, model dfmigrate.Model) error {

	// Create a temp directory to store changed files
	tmpDir, err := ioutil.TempDir(os.TempDir(), "mdspgsql")

	if err != nil {
		return err
	}

	defer os.RemoveAll(tmpDir)

	var fileOperations []fileOp

	for _, migration := range preparedMigrations {

		// Assume it exists if we got to this point
		table, _ := model.GetTableByName(migration.Name)

		log.Printf("Before: %v", table)
		table, err = dfmigrate.ApplyMigration(table, migration)

		if err != nil {
			return err
		}

		log.Printf("After: %v", table)

		entry := df32.FilelistEntry{
			Number:       table.Number,
			DisplayName:  table.DisplayName,
			Name:         table.Name,
			DataflexName: table.Name,
			Driver:       "OTHER",
		}

		if migration.Action == dfmigrate.CreateAction || migration.Action == dfmigrate.AlterAction {

			table.Indexes = createMissingIndexes(table)

			intermediateFile := createIntermediate(driver.generator.schema, table)

			data, err := intermediateFile.MarshalText()

			if err != nil {
				log.Println("couldn't generate intermediate file")
				return err
			}

			fileOperations = append(fileOperations, fileOp{
				operation: "add",
				data:      data,
				filename:  strings.ToLower(entry.Filename()),
			})

			if err := driver.filelist.Set(entry); err != nil {
				log.Println("couldn't update filelist")
				return err
			}

		}

		if migration.Action == dfmigrate.DeleteAction {

			fileOperations = append(fileOperations, fileOp{
				operation: "delete",
				filename:  strings.ToLower(entry.Filename()),
			})

			if err := driver.filelist.Delete(int(entry.Number)); err != nil {
				log.Println("couldn't update filelist")
				return err
			}
		}
	}

	for _, op := range fileOperations {

		targetFilename := getTargetFilePath(driver, op)

		switch op.operation {
		case "add":
			log.Printf("creating %s", targetFilename)
			if targetFilename == "" {
				targetFilename = filepath.Join(".", op.filename)
			}

			err := ioutil.WriteFile(targetFilename, op.data, 0666)
			if err != nil {
				return err
			}
		case "delete":
			if targetFilename != "" {
				log.Printf("deleting %s", targetFilename)
				if err := os.Remove(targetFilename); err != nil {
					return err
				}
			} else {
				return errors.Errorf("could not find file %s", op.filename)
			}
		}
	}

	return nil
}

func (driver *Driver) rollBackOnError(err error, tx *sql.Tx) error {
	if err2 := tx.Rollback(); err2 != nil {
		return errors.Wrap(err, err2.Error())
	}

	return err
}

func (driver *Driver) version(db queryer) (uint64, error) {
	var version uint64
	err := db.QueryRow("SELECT version FROM " + tableName + " ORDER BY version DESC LIMIT 1").Scan(&version)
	switch {
	case err == sql.ErrNoRows:
		return 0, nil
	case err != nil:
		return 0, err
	default:
		return version, nil
	}
}

func (driver *Driver) Version() (uint64, error) {
	return driver.version(driver.db)
}

func (driver *Driver) Baseline(version uint64) error {
	tx, err := driver.db.Begin()

	if err != nil {
		return err
	}

	if _, err := tx.Exec("INSERT INTO "+tableName+" (version) VALUES (?)", version); err != nil {
		return driver.rollBackOnError(err, tx)
	}

	if err := tx.Commit(); err != nil {
		return driver.rollBackOnError(err, tx)
	}

	return nil
}
