package mdspgsql

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pureharvest/df32"
)

func Test_LoadTable(t *testing.T) {

	testDB, cleanup := CreateTestMDSPGSQLDatabase(t, "dataflex")

	defer cleanup()
	defer testDB.Close()

	_, err := testDB.Exec(`
CREATE TABLE "dataflex"."test" (
    "recnum" SERIAL CONSTRAINT "test_index00" UNIQUE,
    "field1" VARCHAR(10) NOT NULL DEFAULT '',
	"field2" CITEXT NOT NULL DEFAULT '',
	"field3" DATE NOT NULL DEFAULT '0001-01-01',
    CONSTRAINT "test001_pk" PRIMARY KEY ("field1")
)
`)
	if err != nil {
		t.Fatal(err)
	}

	intFile := IntermediateFile{
		DatabaseName: "test",
		Schema:       "dataflex",
		Fields: []df32.Field{
			df32.Field{Number: 1, Index: 1},
			df32.Field{Number: 2, Length: 10},
		},
		Indexes: []df32.Index{
			df32.Index{Number: 1, Fields: []df32.IndexField{df32.IndexField{Number: 1}}},
		},
	}

	expected := df32.Table{
		Name:   "test",
		System: false,
		Fields: []df32.Field{
			df32.Field{Number: 1, Name: "field1", Type: df32.AsciiType, Length: 10, Index: 1},
			df32.Field{Number: 2, Name: "field2", Type: df32.AsciiType, Length: 10},
			df32.Field{Number: 3, Name: "field3", Type: df32.DateType},
		},
		Indexes: []df32.Index{
			df32.Index{Number: 1, Name: "test001_pk", PrimaryKey: true, Fields: []df32.IndexField{df32.IndexField{Number: 1, Name: "field1"}}},
		},
	}

	table, err := loadTableFromDB(intFile, testDB.DB)

	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, expected, table)

}
