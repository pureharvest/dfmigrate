package mdspgsql

import (
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/pureharvest/df32"

	"github.com/pkg/errors"
)

// Interfaces to to DB operations

type queryer interface {
	Query(query string, args ...interface{}) (*sql.Rows, error)
	QueryRow(query string, args ...interface{}) *sql.Row
}

type execer interface {
	Exec(query string, args ...interface{}) (*sql.Result, error)
}

type queryExecer interface {
	queryer
	execer
}

type tableLoader struct {
	db   queryer
	path string
}

func (driver tableLoader) LoadTableByName(name string) (df32.Table, error) {

	table := df32.Table{
		Name: name,
	}

	intPath := filepath.Join(driver.path, (strings.ToLower(name) + ".int"))

	if intPath == "" {
		return table, errors.New("could not find intermediate file")
	}

	intFile, err := os.Open(intPath)

	fmt.Println(intPath)

	if err != nil {
		return table, errors.Wrap(err, "could not open intermedaite file")
	}

	defer intFile.Close()

	intermediateFile, err := loadIntermediateFile(intFile)

	if err != nil {
		return table, errors.Wrap(err, "could not read intermedaite file")
	}

	return loadTableFromDB(intermediateFile, driver.db)

}

func convertSQLTypeToDataflex(t string) (df32.Type, error) {
	t = strings.ToLower(t)
	switch t {
	case "ascii":
		return df32.AsciiType, nil
	case "number":
		return df32.NumberType, nil
	case "bytea":
		fallthrough
	case "binary":
		return df32.BinaryType, nil
	case "char":
		fallthrough
	case "citext":
		fallthrough
	case "nchar":
		fallthrough
	case "varchar":
		fallthrough
	case "character varying":
		fallthrough
	case "nvarchar":
		return df32.AsciiType, nil
	case "decimal":
		fallthrough
	case "int":
		fallthrough
	case "smallint":
		fallthrough
	case "numeric":
		return df32.NumberType, nil
	case "text":
		fallthrough
	case "ntext":
		return df32.TextType, nil
	case "date":
		fallthrough
	case "datetime":
		fallthrough
	case "datetime2":
		return df32.DateType, nil
	}

	return "", errors.New("unsupportred type")
}

func loadTableFromDB(intFile IntermediateFile, db queryer) (df32.Table, error) {
	table := df32.Table{
		Name: intFile.DatabaseName,
	}

	rows, err := db.Query(`
	SELECT ORDINAL_POSITION, COLUMN_NAME, UDT_NAME, COLUMN_DEFAULT, NUMERIC_PRECISION, NUMERIC_SCALE, CHARACTER_MAXIMUM_LENGTH
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = $1 AND TABLE_SCHEMA=$2`, intFile.DatabaseName, intFile.Schema)

	if err != nil {
		return table, errors.Wrapf(err, "could not load table information: %#v", intFile)
	}

	var fields []df32.Field

	for rows.Next() {
		var name, dataType, defaultValue sql.NullString
		var number, fieldLength, fieldPrecision int
		var precision, scale, maxLen sql.NullInt64
		if err := rows.Scan(&number, &name, &dataType, &defaultValue, &precision, &scale, &maxLen); err != nil {
			return table, errors.Wrap(err, "could not load table information")
		}

		if strings.EqualFold(name.String, "recnum") {
			continue
		}

		switch dataType.String {
		case "smallint":
			fieldLength = 4
		case "int":
			fieldLength = 6
		case "nchar":
			fallthrough
		case "char":
			fallthrough
		case "varchar":
			fallthrough
		case "character varying":
			fallthrough
		case "nvarchar":
			fieldLength = int(maxLen.Int64)
		case "decimal":
			fallthrough
		case "numeric":
			fieldLength = int(precision.Int64)
			fieldPrecision = int(scale.Int64)
		}

		ft, err := convertSQLTypeToDataflex(dataType.String)

		if err != nil {
			return table, errors.Wrapf(err, "unknown type in %s", dataType.String)
		}

		field := df32.Field{
			Number:    number - 1,
			Name:      name.String,
			Type:      ft,
			Length:    fieldLength,
			Precision: fieldPrecision,
		}

		fields = append(fields, field)
	}

	if rows.Err() != nil {
		return table, rows.Err()
	}

	fmt.Println(fields)

	for _, field := range intFile.Fields {

		if field.Number-1 > len(fields) {
			return table, errors.Errorf("intermediate file refers to a field %d that doesn't exist", field.Number)
		}

		if field.Index != 0 {
			fields[field.Number-1].Index = field.Index
		}

		if field.RelatesTo.File != 0 {
			fields[field.Number-1].RelatesTo = field.RelatesTo
		}

		if field.Length != 0 {
			fields[field.Number-1].Length = field.Length
			fields[field.Number-1].Precision = field.Precision
		}
	}
	table.Fields = fields

	rows, err = db.Query(`
select
dense_rank() OVER (ORDER BY i.relname),
i.relname as IndexName,
ix.indisprimary as IsPrimaryKey,
ix.indisunique as IsUniqueIndex,
a.attname as ColumnName,
row_number() OVER(PARTITION BY i.relname) as ColumnOrder,
CASE WHEN ix.indoption[row_number() OVER(PARTITION BY i.relname)-1] & 1 = 1 THEN 1::boolean ELSE 0::boolean END AS IsDescending
from
pg_class t,
pg_class i,
pg_index ix,
pg_namespace ns,
pg_attribute a
where
t.oid = ix.indrelid
and i.oid = ix.indexrelid
and a.attrelid = t.oid
and t.relnamespace = ns.oid
and a.attnum = any(ix.indkey)
and t.relkind = 'r'
and t.relname = $2
and ns.nspname = $1
and not (lower(a.attname) = 'recnum' and ix.indnatts = 1)
order by
i.relname,
row_number() OVER(PARTITION BY i.relname)
`, intFile.Schema, intFile.DatabaseName)

	if err != nil {
		return table, errors.Wrap(err, "could not read indexes")
	}

	indexes := []df32.Index{}
	currentIndex := df32.Index{}

	for rows.Next() {
		var indexName, columnName string
		var isDescending, isPrimary, isUnique bool
		var indexNumber, columnNumber int

		if err := rows.Scan(&indexNumber, &indexName, &isPrimary, &isUnique, &columnName, &columnNumber, &isDescending); err != nil {
			return table, errors.Wrap(err, "could not load table information")
		}

		if indexNumber > len(indexes) {
			indexes = append(indexes, df32.Index{
				Name:       indexName,
				Number:     indexNumber,
				PrimaryKey: isPrimary,
			})
		}

		indexes[indexNumber-1].Fields = append(currentIndex.Fields, df32.IndexField{
			Name:       columnName,
			Number:     columnNumber,
			Descending: isDescending,
		})

	}

	if rows.Err() != nil {
		return table, errors.Wrap(rows.Err(), "could not read indexes")
	}

	for _, index := range intFile.Indexes {
		if index.Number > len(indexes) {
			return table, errors.New("intermediate file references index that doesnt exist")
		}

		table.Indexes = append(table.Indexes, indexes[index.Number-1])
	}

	return table, nil
}
