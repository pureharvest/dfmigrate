package mdspgsql

import (
	"database/sql"
	"fmt"
)

func Bootstrap(host, user, password, database string) error {

	var connectionString string

	if user == "" || password == "" {
		connectionString = fmt.Sprintf("%s", host)
	} else {
		connectionString = fmt.Sprintf("%s:%s@%s", user, password, host)
	}

	db, err := sql.Open("postgres", "postgres://"+connectionString+"?sslmode=disable")

	if err != nil {
		return err
	}

	if err := db.Ping(); err != nil {
		return err
	}

	if _, err := db.Exec(fmt.Sprintf(`CREATE DATABASE "%s"`, database)); err != nil {
		return err
	}

	return db.Close()
}
