package mdspgsql

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/andybalholm/crlf"
	"github.com/spf13/viper"

	"gitlab.com/pureharvest/df32"
	"gitlab.com/pureharvest/dfmigrate"

	"io/ioutil"

	"github.com/stretchr/testify/assert"
)

type testEnvironment struct {
	config        *viper.Viper
	Env           df32.Environment
	RootPath      string
	MigrationPath string
	ExpectedPath  string
	Cleanup       func()
	TestDB        TestDB
	Migrator      dfmigrate.Migrator
}

func (env testEnvironment) Validate(t *testing.T) {
	t.Log("Checking environment", env.ExpectedPath)
	err := filepath.Walk(env.ExpectedPath, func(path string, info os.FileInfo, err error) error {
		t.Logf("checking file %s", path)

		if path == env.ExpectedPath {
			return nil
		}

		if info.IsDir() {
			return filepath.SkipDir
		}

		srcFile, err := crlf.Open(path)

		if err != nil {
			return err
		}

		defer srcFile.Close()

		destFile, err := crlf.Open(filepath.Join(env.RootPath, filepath.Base(path)))

		if err != nil {
			return err
		}

		defer destFile.Close()

		sData, err := ioutil.ReadAll(srcFile)
		if err != nil {
			return err
		}
		dData, err := ioutil.ReadAll(destFile)
		if err != nil {
			return err
		}

		assert.Equal(t, string(sData), string(dData))

		return nil
	})

	if err != nil {
		env.Cleanup()
		t.Fatal(err)
	}
}

func CreateTestEnvironment(t *testing.T, path string) testEnvironment {

	// Create a temp directory to store changed files
	tmpDir, err := ioutil.TempDir(os.TempDir(), path)

	if err != nil {
		t.Fatal("could not create test environment")
	}

	cleanup := func() {

		if err := os.RemoveAll(tmpDir); err != nil {
			t.Fatal(err)
		}
	}

	testPath := filepath.Join(".", "test-fixtures", path)
	dataPath := filepath.Join(testPath, "data")

	env := testEnvironment{
		RootPath:      tmpDir,
		MigrationPath: filepath.Join(testPath, "migrations"),
		ExpectedPath:  filepath.Join(testPath, "expected"),
	}

	err = filepath.Walk(dataPath, func(path string, info os.FileInfo, err error) error {

		if path == dataPath {
			return nil
		}

		if info.IsDir() {
			return filepath.SkipDir
		}

		// could change this to io.Copy. All files are expected to be small.
		srcFile, err := ioutil.ReadFile(path)

		if err != nil {
			return err
		}

		err = ioutil.WriteFile(filepath.Join(env.RootPath, filepath.Base(path)), srcFile, 0600)

		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		cleanup()
		t.Fatal(err)
	}

	fl, err := df32.NewFilelist(filepath.Join(env.RootPath, "filelist.cfg"))

	if err != nil {
		cleanup()
		t.Fatal(err)
	}

	flDataFile, err := os.Open(filepath.Join(testPath, "filelist.csv"))
	if err == nil {
		defer flDataFile.Close()
		reader := csv.NewReader(flDataFile)

		lines, err := reader.ReadAll()
		if err != nil {
			cleanup()
			t.Fatal(err)
		}

		for _, line := range lines {
			if len(line) != 5 {
				cleanup()
				t.Fatal("invalid filelist data")
			}

			number, err := strconv.Atoi(line[0])
			if err != nil {
				cleanup()
				t.Fatal(err)
			}
			entry := df32.FilelistEntry{
				Number:       int64(number),
				Driver:       line[1],
				Name:         line[2],
				DataflexName: line[3],
				DisplayName:  line[4],
			}
			fl.Set(entry)
		}
	}

	if err := fl.Save(); err != nil {
		cleanup()
		t.Fatal(err)
	}

	env.Env = df32.NewEnvironment(env.RootPath)

	env.Cleanup = cleanup
	env.config = viper.New()
	env.config.Set("migrationsPath", env.MigrationPath)
	env.config.Set("filelist", fl.Filename)
	return env
}

func compareDataDirectories(t *testing.T, src, dest string) {
	err := filepath.Walk(src, func(path string, info os.FileInfo, err error) error {

		if path == src || strings.EqualFold(filepath.Base(path), "filelist.cfg") {
			return nil
		}

		if info.IsDir() {
			return filepath.SkipDir
		}

		srcFile, err := crlf.Open(path)

		if err != nil {
			return err
		}

		defer srcFile.Close()

		destFile, err := crlf.Open(filepath.Join(dest, filepath.Base(path)))

		if err != nil {
			return err
		}

		defer destFile.Close()

		sData, err := ioutil.ReadAll(srcFile)
		if err != nil {
			return err
		}
		dData, err := ioutil.ReadAll(destFile)
		if err != nil {
			return err
		}

		assert.Equal(t, string(sData), string(dData))

		return nil
	})

	if err != nil {
		t.Fatal(err)
	}

}

func createDB(t *testing.T, db *sql.DB, name string) {

	t.Log("Creating Database ", name)
	if _, err := db.Exec(fmt.Sprintf("CREATE DATABASE %s", name)); err != nil {
		t.Fatal(err)
	}

}

func dropDB(t *testing.T, db *sql.DB, name string) {
	t.Log("Dropping Database ", name)
	// if _, err := db.Exec(fmt.Sprintf("ALTER DATABASE [%s] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;", name)); err != nil {
	// 	t.Fatal(err)
	// }

	if _, err := db.Exec(fmt.Sprintf("DROP DATABASE %s", name)); err != nil {
		t.Fatal(err)
	}
}

func randomDBName(prefix string) string {

	rand.Seed(time.Now().UTC().UnixNano())
	const chars = "abcdefghijklmnopqrstuvwxyz0123456789"
	result := make([]byte, 10)
	for i := 0; i < 10; i++ {
		result[i] = chars[rand.Intn(len(chars))]
	}
	return prefix + string(result)

}

type TestDB struct {
	Name             string
	Host             string
	Username         string
	Password         string
	ConnectionString string
	*sql.DB
}

func CreateTestMDSPGSQLDatabase(t *testing.T, schema string) (TestDB, func()) {

	host := os.Getenv("POSTGRES_PORT_5432_TCP_ADDR")
	user := os.Getenv("POSTGRES_USER")
	password := os.Getenv("POSTGRES_PASSWORD")

	if host == "" {
		host = "127.0.0.1"
	}

	databaseName := randomDBName("df")

	connectionString := fmt.Sprintf("postgres://%s:%s@%s/postgres?sslmode=disable", user, password, host)

	t.Log(connectionString)

	db, err := sql.Open("postgres", connectionString)

	if err != nil {
		t.Fatalf("%#v", err)
	}

	if err := db.Ping(); err != nil {
		t.Fatalf("%#v", err)
	}

	createDB(t, db, databaseName)

	testDB := TestDB{
		Name:             databaseName,
		Host:             host,
		Username:         user,
		Password:         password,
		ConnectionString: fmt.Sprintf("%s:%s@%s/%s?sslmode=disable", user, password, host, databaseName),
	}

	// Create separate connection to support Azure sql
	db2, err := sql.Open("postgres", "postgres://"+testDB.ConnectionString)

	if err != nil {
		t.Fatal(err)
	}

	_, err = db2.Exec(fmt.Sprintf("CREATE SCHEMA %s;", schema))

	if err != nil {
		t.Fatal(err)
	}

	_, err = db2.Exec(fmt.Sprintf("CREATE EXTENSION CITEXT;"))

	if err != nil {
		t.Fatal(err)
	}

	testDB.DB = db2

	cleanUp := func() {
		testDB.DB.Close()
		dropDB(t, db, databaseName)
		db.Close()

	}

	return testDB, cleanUp

}
