package mdspgsql

import (
	"bytes"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pureharvest/df32"
)

func Test_IntermediateRead(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected IntermediateFile
		err      error
	}{
		{
			input: `
DRIVER_NAME MDSPGSQL
DATABASE_NAME BLINE

PRIMARY_INDEX 0

INDEX_NUMBER 1
INDEX_SEGMENT_FIELD 1
`,
			expected: IntermediateFile{
				DatabaseName: "BLINE",

				System: false,
				Indexes: []df32.Index{
					df32.Index{Number: 1, Fields: []df32.IndexField{df32.IndexField{Number: 1}}},
				},
			},
		},
		{
			input: `
DRIVER_NAME MDSPGSQL
DATABASE_NAME BLINE

PRIMARY_INDEX 0

FIELD_NUMBER 1
FIELD_INDEX 1

FIELD_NUMBER 6
FIELD_LENGTH 5
FIELD_RELATED_FILE 96
FIELD_RELATED_FIELD 1

INDEX_NUMBER 1
INDEX_SEGMENT_FIELD 1

INDEX_NUMBER 2
INDEX_SEGMENT_FIELD 3
INDEX_SEGMENT_CASE IGNORED
INDEX_SEGMENT_FIELD 1
`,
			expected: IntermediateFile{
				DatabaseName: "BLINE",
				System:       false,
				Fields: []df32.Field{
					df32.Field{Number: 1, Index: 1},
					df32.Field{Number: 6, Length: 5, RelatesTo: df32.FieldRelation{File: 96, Field: 1}},
				},
				Indexes: []df32.Index{
					df32.Index{Number: 1, Fields: []df32.IndexField{df32.IndexField{Number: 1}}},
					df32.Index{Number: 2, Fields: []df32.IndexField{df32.IndexField{Number: 3, Uppercase: true}, df32.IndexField{Number: 1}}},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			file, err := loadIntermediateFile(bytes.NewBufferString(test.input))

			assert.Equal(t, test.err, err)
			assert.Equal(t, test.expected, file)
		})
	}
}

func Test_WriteIntermediateFile(t *testing.T) {
	tests := []struct {
		name     string
		expected string
		input    IntermediateFile
		err      error
	}{
		{
			expected: `DRIVER_NAME MDSPGSQL
DATABASE_NAME BLINE
NAME_SPACE dataflex


INDEX_NUMBER 1
INDEX_NUMBER_SEGMENTS 1
INDEX_SEGMENT_FIELD 1
`,
			input: IntermediateFile{
				DatabaseName: "BLINE",
				Schema:       "dataflex",
				System:       false,
				Indexes: []df32.Index{
					df32.Index{Number: 1, Fields: []df32.IndexField{df32.IndexField{Number: 1}}},
				},
			},
		},
		{
			expected: `DRIVER_NAME MDSPGSQL
DATABASE_NAME BLINE
NAME_SPACE dataflex


FIELD_NUMBER 1
FIELD_INDEX 1

FIELD_NUMBER 6
FIELD_RELATED_FILE 96
FIELD_RELATED_FIELD 1


INDEX_NUMBER 1
INDEX_NUMBER_SEGMENTS 1
INDEX_SEGMENT_FIELD 1

INDEX_NUMBER 2
INDEX_NUMBER_SEGMENTS 2
INDEX_SEGMENT_FIELD 3
INDEX_SEGMENT_CASE IGNORED
INDEX_SEGMENT_FIELD 1
`,
			input: IntermediateFile{
				DatabaseName: "BLINE",
				Schema:       "dataflex",
				System:       false,
				Fields: []df32.Field{
					df32.Field{Number: 1, Index: 1},
					df32.Field{Number: 6, Length: 5, RelatesTo: df32.FieldRelation{File: 96, Field: 1}},
				},
				Indexes: []df32.Index{
					df32.Index{Number: 1, Fields: []df32.IndexField{df32.IndexField{Number: 1}}},
					df32.Index{Number: 2, Fields: []df32.IndexField{df32.IndexField{Number: 3, Uppercase: true}, df32.IndexField{Number: 1}}},
				},
			},
		},
		{
			name: "fields that don't require intermediate values",
			expected: `DRIVER_NAME MDSPGSQL
DATABASE_NAME BLINE
NAME_SPACE dataflex


FIELD_NUMBER 1
FIELD_INDEX 1

FIELD_NUMBER 6
FIELD_RELATED_FILE 96
FIELD_RELATED_FIELD 1


INDEX_NUMBER 1
INDEX_NUMBER_SEGMENTS 1
INDEX_SEGMENT_FIELD 1

INDEX_NUMBER 2
INDEX_NUMBER_SEGMENTS 2
INDEX_SEGMENT_FIELD 3
INDEX_SEGMENT_CASE IGNORED
INDEX_SEGMENT_FIELD 1
`,
			input: IntermediateFile{
				DatabaseName: "BLINE",
				Schema:       "dataflex",
				System:       false,
				Fields: []df32.Field{
					df32.Field{Number: 1, Index: 1},
					df32.Field{Number: 2, Type: df32.AsciiType},
					df32.Field{Number: 6, Type: df32.AsciiType, Length: 5, RelatesTo: df32.FieldRelation{File: 96, Field: 1}},
				},
				Indexes: []df32.Index{
					df32.Index{Number: 1, Fields: []df32.IndexField{df32.IndexField{Number: 1}}},
					df32.Index{Number: 2, Fields: []df32.IndexField{df32.IndexField{Number: 3, Uppercase: true}, df32.IndexField{Number: 1}}},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			var buf bytes.Buffer

			err := writeIntermediateFile(test.input, &buf)

			assert.Equal(t, test.err, err)
			assert.Equal(t, strings.Split(test.expected, "\n"), strings.Split(buf.String(), "\r\n"))
		})
	}
}

func Test_CreateIntermediate(t *testing.T) {
	tests := []struct {
		name     string
		schema   string
		table    df32.Table
		expected IntermediateFile
	}{
		{
			table: df32.Table{
				Name:   "test",
				System: false,
				Fields: []df32.Field{
					df32.Field{Number: 1, Name: "field1", Type: df32.AsciiType, Length: 10, Index: 1},
					df32.Field{Number: 2, Name: "field2", Type: df32.AsciiType, Length: 10},
					df32.Field{Number: 3, Name: "field3", Type: df32.DateType},
				},
				Indexes: []df32.Index{
					df32.Index{Number: 1, Name: "test001_pk", PrimaryKey: true, Fields: []df32.IndexField{df32.IndexField{Number: 1, Name: "field1"}}},
				},
			},
			schema: "dataflex",
			expected: IntermediateFile{
				Encoding:     8,
				LocalCache:   true,
				Optimize:     "FOR_SET",
				MaxRows:      10,
				DatabaseName: "test",
				Schema:       "dataflex",
				Fields: []df32.Field{
					df32.Field{Number: 1, Name: "field1", Type: df32.AsciiType, Length: 10, Index: 1},
					df32.Field{Number: 2, Name: "field2", Type: df32.AsciiType, Length: 10},
					df32.Field{Number: 3, Name: "field3", Type: df32.DateType},
				},
				Indexes: []df32.Index{
					df32.Index{Number: 1, Name: "test001_pk", PrimaryKey: true, Fields: []df32.IndexField{df32.IndexField{Number: 1, Name: "field1"}}},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			intFile := createIntermediate(test.schema, test.table)

			assert.Equal(t, test.expected, intFile)
		})
	}
}
