package mdspgsql

import (
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/pureharvest/df32"
	"gitlab.com/pureharvest/dfmigrate"
)

func GenerateTableMigrationScript(model dfmigrate.Model, migration dfmigrate.TableMigration, g MDSPGSQLGenerator) ([]string, error) {

	var statements []string

	tableName := migration.Name

	schema := g.schema

	switch migration.Action {
	case dfmigrate.CreateAction:
		table := migration.Table()

		table.Indexes = createMissingIndexes(table)

		statements = append(statements, g.CreateTable(table))

		for _, index := range table.Indexes {
			if !index.PrimaryKey {
				statements = append(statements, g.CreateIndex(schema, tableName, index.Name, index.Fields))
			}

		}

	case dfmigrate.AlterAction:

		table, found := model.GetTableByName(migration.Name)

		if !found {
			return nil, errors.New("table does not exist")
		}

		fields := migration.Fields

		for _, field := range fields {
			switch field.Action {
			case dfmigrate.AlterAction:
				if isRename(field) {
					statements = append(statements, g.RenameField(schema, tableName, field.Name, field.NewName))
				}

				if isTypeChange(field) {
					statements = append(statements, g.AlterField(schema, tableName, field.Name, field.Field(), fieldIgnoresCase(field.Field(), table.Indexes)))
				}
			case dfmigrate.CreateAction:
				statements = append(statements, g.AddField(schema, tableName, field.Field(), fieldIgnoresCase(field.Field(), table.Indexes)))
			case dfmigrate.DeleteAction:
				statements = append(statements, g.DropField(schema, tableName, field.Name))
			}

		}
	case dfmigrate.DeleteAction:
		statements = append(statements, g.DropTable(migration.Name))
	}

	return statements, nil
}

func isRename(field dfmigrate.FieldMigration) bool {
	return field.NewName != ""
}

func isTypeChange(field dfmigrate.FieldMigration) bool {
	return field.Type != "" || (field.Length != 0 || field.Precision != 0)
}

func createMissingIndexes(table df32.Table) []df32.Index {

	var hasRecnumIndex, hasClustered, hasPrimaryKey bool
	var indexes []df32.Index

	tableName := table.Name

	for _, index := range table.Indexes {
		if strings.EqualFold(index.Fields[0].Name, "RECNUM") {
			hasRecnumIndex = true
		}

		if index.PrimaryKey {
			hasPrimaryKey = true
		}

		indexes = append(indexes, index)
	}

	if !hasRecnumIndex && !table.System {
		recnumIndex := df32.Index{
			Name: tableName + "000",
			Fields: []df32.IndexField{
				{Name: "RECNUM"},
			},
		}

		if !hasPrimaryKey && !hasClustered {
			recnumIndex.Name = recnumIndex.Name + "_PK"
			recnumIndex.PrimaryKey = true
		}

		indexes = append([]df32.Index{recnumIndex}, indexes...)
	}

	for i := range indexes {
		indexes[i].Number = i
	}

	return indexes
}
