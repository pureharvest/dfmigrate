package mdspgsql

import (
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"

	"fmt"

	"bytes"

	"gitlab.com/pureharvest/df32"
)

type IntermediateFile struct {
	DatabaseName       string `json:"database_name"`
	Server             string
	Schema             string
	System             bool
	PrimaryIndexNumber int
	Encoding           int
	Optimize           string
	MaxRows            int
	LocalCache         bool
	Fields             []df32.Field
	Indexes            []df32.Index
}

// const intTemplate = `DRIVER_NAME MDSPGSQL
// DATABASE_NAME {{upper .DatabaseName}}
// NAME_SPACE {{.Schema }}
// {{- if .System }}
// SYSTEM_FILE YES{{ end }}
// {{- if .PrimaryIndexNumber }}
// PRIMARY_INDEX {{.PrimaryIndexNumber}}{{end}}
// {{- if .Encoding }}
// ENCODING_ID {{ .Encoding }}{{ end }}
// {{- if .Optimize }}
// OPTIMIZE {{ upper .Optimize }}{{ end }}
// {{- if .MaxRows }}
// MAX_ROWS_TO_QUERY {{ .MaxRows }}{{ end }}
// {{- if .LocalCache }}
// LOCAL_CACHE YES{{ end }}

// {{ range $i, $field := .Fields -}}
// {{- fieldIntermediate . -}}
// {{- end -}}
// {{- range $i, $index := .Indexes }}

// INDEX_NUMBER {{ .Number }}
// INDEX_NUMBER_SEGMENTS {{ len .Fields }}
// 	{{- range $field := .Fields }}
// INDEX_SEGMENT_FIELD {{ .Number }}
// {{- if .Uppercase }}
// INDEX_SEGMENT_CASE IGNORED
// 	{{- end -}}
// 	{{- end -}}
// {{- end }}
// `

// var funcMap = template.FuncMap{
// 	"upper":             strings.ToUpper,
// 	"lower":             strings.ToLower,
// 	"fieldIntermediate": fieldIntermediate,
// }

// var fileTemplate = template.Must(template.New("int").Funcs(funcMap).Parse(intTemplate))

type printer struct {
	buf bytes.Buffer
	err error
}

func (p *printer) Printf(format string, args ...interface{}) {
	if p.err != nil {
		return
	}

	_, err := fmt.Fprintf(&p.buf, format, args...)

	if err != nil {
		p.err = err
	}
}

func (p *printer) Println(args ...interface{}) {
	for _, a := range args {
		p.Printf("%v ", a)
	}

	p.Printf("\r\n")
}

func (p *printer) PrintField(field, value string) {
	p.Printf("%s %s\r\n", strings.ToUpper(field), value)
}

func (i IntermediateFile) MarshalText() ([]byte, error) {
	var p printer

	p.PrintField("DRIVER_NAME", "MDSPGSQL")
	p.PrintField("DATABASE_NAME", strings.ToUpper(i.DatabaseName))
	p.PrintField("NAME_SPACE", i.Schema)
	if i.System {
		p.PrintField("SYSTEM_FILE", "YES")
	}
	if i.PrimaryIndexNumber != 0 {
		p.PrintField("PRIMARY_INDEX", strconv.Itoa(i.PrimaryIndexNumber))
	}
	if i.Encoding != 0 {
		p.PrintField("ENCODING_ID", strconv.Itoa(i.Encoding))
	}
	if i.Optimize != "" {
		p.PrintField("OPTIMIZE", i.Optimize)
	}
	if i.MaxRows != 0 {
		p.PrintField("MAX_ROWS_TO_QUERY", strconv.Itoa(i.MaxRows))
	}
	if i.LocalCache {
		p.PrintField("LOCAL_CACHE", "YES")
	}

	p.Println()

	for _, field := range i.Fields {
		p.Printf("%s", fieldIntermediate(field, i.Indexes))
	}

	if len(i.Fields) > 0 {
		p.Println()
	}

	for _, index := range i.Indexes {
		p.Println()
		p.PrintField("INDEX_NUMBER", strconv.Itoa(index.Number))
		p.PrintField("INDEX_NUMBER_SEGMENTS", strconv.Itoa(len(index.Fields)))
		for _, field := range index.Fields {
			p.PrintField("INDEX_SEGMENT_FIELD", strconv.Itoa(field.Number))
			if field.Uppercase {
				p.PrintField("INDEX_SEGMENT_CASE", "IGNORED")
			}
		}

	}

	return p.buf.Bytes(), p.err

}

func (i *IntermediateFile) UnmarshalText(text []byte) error {
	var err error

	*i, err = loadIntermediateFile(bytes.NewReader(text))

	return err
}

func fieldIgnoresCase(f df32.Field, indexes []df32.Index) bool {

	for _, index := range indexes {
		for _, indexField := range index.Fields {
			if indexField.Number == f.Number && indexField.Uppercase {
				return true
			}
		}
	}

	return false
}

func fieldNeedsIntermediate(f df32.Field, indexes []df32.Index) bool {
	return f.RelatesTo.File != 0 ||
		f.Index != 0 ||
		f.Type == df32.NumberType ||
		(f.Type == df32.AsciiType && fieldIgnoresCase(f, indexes))
}

func fieldIntermediate(f df32.Field, indexes []df32.Index) string {

	if !fieldNeedsIntermediate(f, indexes) {
		return ""
	}

	output := fmt.Sprintf("\r\nFIELD_NUMBER %d", f.Number)

	switch f.Type {
	// case df32.Ascii:
	// 	if fieldIgnoresCase(f, indexes) {
	// 		output += fmt.Sprintf("\r\nFIELD_LENGTH %d", f.Length)
	// 	}
	case df32.NumberType:
		if f.Precision == 0 {
			output += fmt.Sprintf("\r\nFIELD_LENGTH %d", f.Length)
		}
	}

	if f.Index != 0 {
		output += fmt.Sprintf("\r\nFIELD_INDEX %d", f.Index)
	}

	if f.RelatesTo.File != 0 {
		output += fmt.Sprintf("\r\nFIELD_RELATED_FILE %d\r\nFIELD_RELATED_FIELD %d", f.RelatesTo.File, f.RelatesTo.Field)
	}

	return output + "\r\n"
}

func createIntermediate(schema string, table df32.Table) IntermediateFile {
	indexes := make([]df32.Index, 0, len(table.Indexes))

	//Remove the recnum index
	for _, index := range table.Indexes {
		if len(index.Fields) == 1 && strings.EqualFold(index.Fields[0].Name, "recnum") {
			continue
		}

		indexes = append(indexes, index)
	}

	intFile := IntermediateFile{
		DatabaseName: table.Name,
		Schema:       schema,
		System:       table.System,
		Encoding:     8,
		MaxRows:      10,
		Optimize:     "FOR_SET",
		LocalCache:   true,
		Fields:       table.Fields,
		Indexes:      indexes,
	}

	return intFile
}

func writeIntFile(t IntermediateFile, filename string) error {
	file, err := os.Create(filename)

	if err != nil {
		return err
	}
	defer file.Close()

	return writeIntermediateFile(t, file)
}

func writeIntermediateFile(t IntermediateFile, wr io.Writer) error {

	data, err := t.MarshalText()

	if err != nil {
		return err
	}

	_, err = wr.Write(data)

	return err

}

func loadIntermediateFile(r io.Reader) (IntermediateFile, error) {
	intTable := IntermediateFile{}

	config, err := df32.ReadIntermediate(r)

	if err != nil {
		return intTable, errors.Wrap(err, "can't load table int file")
	}

	// Use pointer to tell if we have got a def
	var currentField *df32.Field
	var currentIndex *df32.Index
	var currentIndexField *df32.IndexField

	for _, item := range config {
		switch strings.ToUpper(item.Name) {
		case "SERVER_NAME":
			intTable.Server = item.Value
		case "DATABASE_NAME":
			intTable.DatabaseName = item.Value
		case "NAME_SPACE":
			intTable.Schema = item.Value
		case "SYSTEM_FILE":
			if strings.ToUpper(item.Value) == "YES" {
				intTable.System = true
			}
		case "PRIMARY_INDEX":
			index, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "primary index must be a number")
			}
			intTable.PrimaryIndexNumber = index
		case "FIELD_NUMBER":
			if currentIndex != nil {
				// TODO: Validate index config
				intTable.Indexes = append(intTable.Indexes, *currentIndex)
				currentIndex = nil
			}
			fieldNum, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field number must be a number")
			}

			if currentField == nil {
				currentField = &df32.Field{
					Number: fieldNum,
				}
			}

			if currentField.Number != fieldNum {
				// TODO: Validate field config
				intTable.Fields = append(intTable.Fields, *currentField)
				currentField = &df32.Field{
					Number: fieldNum,
				}
			}
		case "FIELD_INDEX":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.Number)
			}
			index, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field index must be a number")
			}

			currentField.Index = index
		case "FIELD_LENGTH":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.Number)
			}
			length, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field index must be a number")
			}

			currentField.Length = length
		case "FIELD_PRECISION":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.Number)
			}
			precision, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field index must be a number")
			}

			currentField.Precision = precision
		case "FIELD_RELATED_FILE":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.Number)
			}
			fileNum, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field related file must be a number")
			}

			currentField.RelatesTo.File = fileNum
		case "FIELD_RELATED_FIELD":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.Number)
			}
			fieldNum, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field related field must be a number")
			}

			currentField.RelatesTo.Field = fieldNum
		case "FIELD_TYPE":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.Number)
			}

			switch strings.ToLower(item.Value) {
			case "0":
				currentField.Type = df32.AsciiType
			}

		case "INDEX_NUMBER":
			if currentField != nil {
				// TODO: Validate field config
				intTable.Fields = append(intTable.Fields, *currentField)
				currentField = nil
			}
			index, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "index number must be a number")
			}

			if currentIndex == nil {
				currentIndex = &df32.Index{
					Number: index,
				}
			}

			if currentIndex.Number != index {
				if currentIndexField != nil {
					currentIndex.Fields = append(currentIndex.Fields, *currentIndexField)
					currentIndexField = nil
				}
				// TODO: Validate index config
				intTable.Indexes = append(intTable.Indexes, *currentIndex)
				currentIndex = &df32.Index{
					Number: index,
				}
			}

		case "INDEX_NAME":
			if currentIndex == nil {
				return intTable, errors.Errorf("index properties must come after index numbers. (%d)", item.Number)
			}

			currentIndex.Name = item.Value
		case "INDEX_NUMBER_SEGMENTS":
		case "INDEX_NATIVE_CREATED":
		case "INDEX_SEGMENT_FIELD":
			if currentIndex == nil {
				return intTable, errors.Errorf("index properties must come after index numbers. (%d)", item.Number)
			}

			if currentIndexField != nil {
				currentIndex.Fields = append(currentIndex.Fields, *currentIndexField)
			}

			index, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "index segment field must be a number")
			}

			currentIndexField = &df32.IndexField{
				Number: index,
			}
		case "INDEX_SEGMENT_CASE":
			if currentIndexField == nil {
				return intTable, errors.Errorf("index segment properties must come after index segment numbers. (%d)", item.Number)
			}

			if strings.EqualFold(item.Value, "IGNORED") {
				currentIndexField.Uppercase = true
			}
		}
	}

	if currentIndexField != nil {
		currentIndex.Fields = append(currentIndex.Fields, *currentIndexField)
	}

	if currentField != nil {
		// TODO: Validate field config
		intTable.Fields = append(intTable.Fields, *currentField)
		currentField = nil
	}

	if currentIndex != nil {
		// TODO: Validate field config
		intTable.Indexes = append(intTable.Indexes, *currentIndex)
		currentIndex = nil
	}

	return intTable, nil
}
