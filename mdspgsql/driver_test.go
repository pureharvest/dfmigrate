package mdspgsql

import (
	"database/sql"
	"log/slog"
	"os"
	"path/filepath"
	"testing"

	"github.com/pkg/errors"
	"gitlab.com/pureharvest/dfmigrate"
)

func setupTest(t *testing.T, name string) (dfmigrate.TestEnvironment, TestDB, func()) {

	// Load environment
	env := dfmigrate.CreateTestEnvironment(t, name)

	// Create test database
	testDB, cleanup := CreateTestMDSPGSQLDatabase(t, "dataflex")

	// Initialize migrator
	migrator := dfmigrate.Migrator{
		Log: slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
			Level: slog.LevelDebug,
		})),
	}

	if err := migrator.Register("mdspgsql", &Driver{}); err != nil {
		cleanup()
		env.Cleanup()
		t.Fatal(err)
	}

	options := dfmigrate.Options{
		Driver:              "mdspgsql",
		URL:                 testDB.ConnectionString,
		IntermediatePath:    env.RootPath,
		FileDefinitionsPath: env.RootPath,
		FilelistPath:        filepath.Join(env.RootPath, "filelist.cfg"),
		MigrationsPath:      env.MigrationPath,
	}

	if err := migrator.Initialize(options); err != nil {
		cleanup()
		env.Cleanup()
		t.Fatal(err)
	}

	env.Migrator = migrator

	oldCleanup := env.Cleanup
	env.Cleanup = func() {
		env.Migrator.Close()
		cleanup()
		oldCleanup()
	}

	return env, testDB, env.Cleanup

}

func Test_CreateMigration(t *testing.T) {

	env, testDB, cleanup := setupTest(t, "create")

	defer cleanup()

	if err := env.Migrator.Up(); err != nil {
		t.Fatalf("%#v\n%#v", err, env)
	}

	if err := env.Migrator.GenerateFileDefinitions(1); err != nil {
		t.Fatalf("%#v\n%#v", err, env)
	}

	assertTableExists(t, testDB.DB, "dataflex", "BLINE")

	env.Validate(t)

}

func assertTableExists(t *testing.T, db *sql.DB, schema, name string) {
	var exists int

	row := db.QueryRow(`SELECT 1 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = $1
                 AND  TABLE_NAME = $2`, schema, name)

	if err := row.Scan(&exists); err != nil {
		t.Fatalf("expected %s.%s to exist: %v", schema, name, err)
	}

}

func Test_AlterMigration(t *testing.T) {
	env, _, cleanup := setupTest(t, "alter")

	defer cleanup()

	if err := env.Migrator.Up(); err != nil {
		t.Fatalf("%#v", errors.Cause(err))
	}

	if err := env.Migrator.GenerateFileDefinitions(2); err != nil {
		t.Fatalf("%#v", errors.Cause(err))
	}

	env.Validate(t)
}

func Test_FailedMigration(t *testing.T) {
	env, testDB, cleanup := setupTest(t, "failed")

	defer cleanup()

	if _, err := testDB.Exec(`CREATE TABLE "dataflex"."BLINE" (
    "RECNUM" SERIAL CONSTRAINT "BLINE_INDEX00" UNIQUE,
    "NUMBER" VARCHAR(10) NOT NULL DEFAULT ('')
);`); err != nil {
		t.Fatal(err)
	}

	if err := env.Migrator.Up(); err == nil {
		t.Fatalf("expected an error")
	}

}
