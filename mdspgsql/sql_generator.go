package mdspgsql

import (
	"fmt"
	"strings"

	"gitlab.com/pureharvest/df32"
)

var defaultMDSPGSQLGenerator = MDSPGSQLGenerator{
	schema:   "dataflex",
	types:    defaultMDSPGSQLTypes,
	defaults: defaultValues,
}

var defaultMDSPGSQLTypes = df32.TypeMap{
	df32.AsciiType:  "VARCHAR",
	df32.NumberType: "NUMERIC",
	df32.TextType:   "TEXT",
	df32.BinaryType: "BYTEA",
	df32.DateType:   "DATE",
	//Non-default for legacy
	"char":     "VARCHAR",
	"datetime": "DATE",
}

var defaultValues = map[df32.Type]string{
	df32.AsciiType:  "''",
	df32.NumberType: "0",
	df32.TextType:   "''",
	df32.BinaryType: "e'\\000'::bytea",
	df32.DateType:   "'0001-01-01'",
	"char":          "''",
	"datetime":      "'0001-01-01'",
}

type MDSPGSQLGenerator struct {
	schema   string
	types    df32.TypeMap
	defaults map[df32.Type]string
}

func sqlLength(f df32.Field, ignoreCase bool) string {
	if f.Type == df32.DateType || f.Type == df32.TextType || f.Type == "datetime" || (f.Type == df32.AsciiType && ignoreCase) {
		return ""
	}

	if f.Type == df32.NumberType {
		if f.Precision == 0 {
			return fmt.Sprintf("(%d)", f.Length)
		}

		return fmt.Sprintf("(%d,%d)", f.Length, f.Precision)
	}

	return fmt.Sprintf("(%d)", f.Length)
}

func (g MDSPGSQLGenerator) typeDefinition(field df32.Field, ignoreCase bool) string {

	return g.ConvertType(field.Type, ignoreCase) + sqlLength(field, ignoreCase)
}

func (g MDSPGSQLGenerator) fieldDefinition(table string, field df32.Field, ignoreCase bool) string {

	// Special case for identity fields
	if strings.EqualFold(field.Name, "recnum") {
		return `"RECNUM" SERIAL`
	}

	return fmt.Sprintf(
		`"%s" %s NOT NULL DEFAULT %s`,
		strings.ToUpper(field.Name),
		g.typeDefinition(field, ignoreCase),
		g.GetDefault(field.Type),
	)
}

func (g MDSPGSQLGenerator) ConvertType(t df32.Type, ignoreCase bool) string {

	// Special case for case-insensitive fields
	if ignoreCase && t == df32.AsciiType {
		return "citext"
	}

	sqlType := g.types[t]

	if sqlType == "" {
		sqlType = defaultMDSPGSQLTypes[t]
	}

	return sqlType
}

func (g MDSPGSQLGenerator) GetDefault(t df32.Type) string {

	defaultValue := g.defaults[t]

	if defaultValue == "" {
		return defaultValues[t]
	}

	return defaultValue
}

func mdspgsqlIndexFieldsDefinition(fields []df32.IndexField) string {
	fieldDefs := make([]string, 0, len(fields))

	for _, field := range fields {
		direction := "ASC"

		if field.Descending {
			direction = "DESC"
		}

		fieldDefs = append(fieldDefs, fmt.Sprintf(`"%s" %s`, strings.ToUpper(field.Name), direction))
	}

	return strings.Join(fieldDefs, ",")
}

func mdspgsqlClusteredIndex(table string, indexes []df32.Index) string {
	var name string
	var fields []df32.IndexField

	for _, index := range indexes {
		if index.PrimaryKey {
			name = index.Name
			fields = index.Fields
			break
		}
	}

	if name == "" {
		return fmt.Sprintf(`CONSTRAINT "%s_INDEX00" PRIMARY KEY ("RECNUM")`, strings.ToUpper(table))
	}

	fieldDefs := make([]string, 0, len(fields))

	for _, field := range fields {
		fieldDefs = append(fieldDefs, strings.ToUpper(field.Name))
	}

	return fmt.Sprintf(`CONSTRAINT "%s_INDEX00" UNIQUE ("RECNUM"),
    CONSTRAINT "%s" PRIMARY KEY ("%s")`, strings.ToUpper(table), strings.ToUpper(name), strings.Join(fieldDefs, `", "`))
}

func (g MDSPGSQLGenerator) CreateIndex(schema, table, name string, fields []df32.IndexField) string {

	return fmt.Sprintf(`CREATE UNIQUE INDEX "%s" ON "%s"."%s" (%s)`, strings.ToUpper(name), schema, strings.ToUpper(table), mdspgsqlIndexFieldsDefinition(fields))

}

func (g MDSPGSQLGenerator) CreateTable(table df32.Table) string {
	name := table.Name
	fields := table.Fields
	schema := g.schema
	indexes := table.Indexes

	fieldDefs := make([]string, 0, len(fields)+1)

	if len(fields) > 0 && fields[0].Name != "RECNUM" {
		fieldDefs = append(fieldDefs, g.fieldDefinition(name, df32.Field{
			Name: "RECNUM",
		}, true))
	}
	for _, field := range fields {
		fieldDefs = append(fieldDefs, g.fieldDefinition(name, field, fieldIgnoresCase(field, table.Indexes)))
	}

	primaryKey := mdspgsqlClusteredIndex(name, indexes)

	if primaryKey != "" {
		primaryKey = ",\n    " + primaryKey
	}

	tableStatement := fmt.Sprintf(`CREATE TABLE "%s"."%s" (
    %s%s
) WITH (
    OIDS = TRUE
)`, schema, strings.ToUpper(name), strings.Join(fieldDefs, ",\n    "), primaryKey)

	return tableStatement
}

func (g MDSPGSQLGenerator) DropTable(table string) string {
	return fmt.Sprintf(`DROP TABLE "%s"."%s"`, g.schema, strings.ToUpper(table))
}

func (g MDSPGSQLGenerator) AddField(schema, table string, field df32.Field, ignoresCase bool) string {
	return fmt.Sprintf(`ALTER TABLE "%s"."%s" ADD %s`, schema, strings.ToUpper(table), g.fieldDefinition(table, field, ignoresCase))
}

func (g MDSPGSQLGenerator) AlterField(schema, table, from string, to df32.Field, ignoresCase bool) string {

	return fmt.Sprintf(`ALTER TABLE "%s"."%s" ALTER COLUMN "%s" %s`, schema, strings.ToUpper(table), strings.ToUpper(from), g.typeDefinition(to, ignoresCase))
}

func (g MDSPGSQLGenerator) DropField(schema, table, field string) string {
	return fmt.Sprintf(`ALTER TABLE "%s"."%s" DROP COLUMN "%s"`, schema, strings.ToUpper(table), strings.ToUpper(field))
}

func (g MDSPGSQLGenerator) RenameField(schema, table, from, to string) string {
	return fmt.Sprintf(`ALTER TABLE "%s"."%s" RENAME COLUMN "%s" TO "%s"`, schema, strings.ToUpper(table), strings.ToUpper(from), strings.ToUpper(to))
}
