package mssqldrv

import (
	"database/sql"
	"fmt"
	neturl "net/url"
	"strings"

	"github.com/pkg/errors"
)

func parseConnectionString(url string) (connectionString string, options neturl.Values, err error) {

	if i := strings.Index(url, "?"); i != -1 {
		options, err = neturl.ParseQuery(url[i+1:])
		if err != nil {
			return "", nil, errors.Wrap(err, "invalid connection string")
		}

		url = url[:i]
	}

	dfOptions := neturl.Values{}

	for _, option := range []string{"dfpath", "fdpath", "intpath", "applyonly"} {
		if options.Get(option) != "" {
			dfOptions[option] = options[option]
			options.Del(option)
		}
	}

	connectionString = fmt.Sprintf("sqlserver%s?%s", strings.TrimPrefix(url, driverName), options.Encode())

	return connectionString, dfOptions, err
}

func convertToDataflexConnectionString(url string) (string, error) {
	u, err := neturl.Parse(url)

	if err != nil {
		return "", err
	}

	connectionString := "SERVER=" + u.Host

	if u.Path != "" && u.Path != "/" {
		connectionString += "\\" + u.Path
	}

	if u.User != nil && u.User.Username() != "" {
		if username := u.User.Username(); username != "" {
			connectionString += ";UID=" + username
		}

		if password, set := u.User.Password(); set {
			connectionString += ";PWD=" + password
		}

	} else {
		connectionString += ";Trusted_Connection=yes"
	}

	if database := u.Query().Get("database"); database != "" {
		connectionString += ";Database=" + database
	}

	return connectionString, nil
}

func ensureVersionTableExists(db *sql.DB) error {
	if _, err := db.Exec(fmt.Sprintf(`
	if not exists (
		select 1 from sys.tables t join sys.schemas s on (t.schema_id = s.schema_id) where s.name = 'dbo' and t.name = '%s') 
	create table [dbo].[%s] (version int not null primary key);`, tableName, tableName)); err != nil {
		return err
	}
	return nil
}

func ensureSchemaExists(db *sql.DB, schema string) error {
	if _, err := db.Exec(fmt.Sprintf(`
if not exists (
		select 1 from INFORMATION_SCHEMA.SCHEMATA where SCHEMA_NAME = '%s'
) exec (N'CREATE SCHEMA [%s]');
`, schema, schema)); err != nil {
		return err
	}
	return nil
}
