package mssqldrv

import (
	"html/template"
	"io"
	"strconv"
	"strings"

	"golang.org/x/text/transform"

	"github.com/andybalholm/crlf"
	"github.com/pkg/errors"

	"bytes"

	"io/ioutil"

	"gitlab.com/pureharvest/df32"
)

type Intermediate struct {
	DatabaseName       string `json:"database_name"`
	Server             string
	Schema             string
	Recnum             bool
	System             bool
	PrimaryIndexNumber int
	RecordIdentity     string
	Format             string
	UseDummyZeroDate   bool
	Fields             []df32.Field
	Indexes            []df32.Index
}

const intTemplate = `DRIVER_NAME MSSQLDRV
SERVER_NAME {{.Server}}
DATABASE_NAME {{.DatabaseName}}
SCHEMA_NAME {{.Schema}}

{{ if .System }}SYSTEM_FILE YES
{{- else -}}
{{ if .Recnum }}RECNUM_TABLE YES{{ end }}
PRIMARY_INDEX {{.PrimaryIndexNumber}}
GENERATE_RECORD_ID_METHOD {{ if .RecordIdentity }}{{ .RecordIdentity }}{{ else }}IDENTITY_COLUMN{{end}}
{{- end }}
TABLE_CHARACTER_FORMAT {{ if .Format }}{{ .Format }}{{ else }}ANSI{{end}}
{{ if .UseDummyZeroDate }}USE_DUMMY_ZERO_DATE YES{{ end }}
{{- range $i, $field := .Fields -}}
{{- if .NeedsIntermediate }}

FIELD_NUMBER {{ .Number }}
{{- if .NeedsIntermediateLength }}
FIELD_LENGTH {{ .Length }}
{{- end -}}
{{- if .Index }}
FIELD_INDEX {{ .Index }}
{{- end -}}
{{- if .RelatesTo.File }}
FIELD_RELATED_FILE {{.RelatesTo.File}}
FIELD_RELATED_FIELD {{.RelatesTo.Field}}
{{- end -}}
{{- end -}}
{{- end -}}
{{ "" }}
{{- range $i, $index := .Indexes }}

INDEX_NUMBER {{ .Number }}
INDEX_NAME {{ .Name }}
{{- end }}
`

var funcMap = template.FuncMap{
	"upper": strings.ToUpper,
}

var fileTemplate = template.Must(template.New("int").Funcs(funcMap).Parse(intTemplate))

func CreateIntermediate(schema string, table df32.Table) Intermediate {

	table.Indexes = createMissingIndexes(table)

	return Intermediate{
		DatabaseName:     table.Name,
		Schema:           schema,
		RecordIdentity:   "IDENTITY_COLUMN",
		UseDummyZeroDate: true,
		Format:           "ANSI",
		System:           table.System,
		Recnum:           !table.System,
		Server:           "DFCONNID=POSONA",
		Fields:           table.Fields,
		Indexes:          table.Indexes,
	}
}

func (i Intermediate) MarshalText() ([]byte, error) {
	var buf bytes.Buffer

	t := transform.NewWriter(&buf, crlf.ToCRLF{})

	err := fileTemplate.Execute(t, i)

	return buf.Bytes(), err
}

func (i *Intermediate) UnmarshalText(text []byte) error {
	var err error

	*i, err = loadIntermediateFile(bytes.NewReader(text))

	return err
}

func writeIntermediateFile(t Intermediate, filename string) error {
	buf, err := t.MarshalText()

	if err != nil {
		return err
	}

	return ioutil.WriteFile(filename, buf, 0644)

}

func loadIntermediateFile(r io.Reader) (Intermediate, error) {
	intTable := Intermediate{}

	config, err := df32.ReadIntermediate(r)

	if err != nil {
		return intTable, errors.Wrap(err, "can't load table int file")
	}

	// Use pointer to tell if we have got a def
	var currentField *df32.Field
	var currentIndex *df32.Index

	for _, item := range config {
		switch strings.ToUpper(item.Name) {
		case "SERVER_NAME":
			intTable.Server = item.Value
		case "DATABASE_NAME":
			intTable.DatabaseName = item.Value
		case "SCHEMA_NAME":
			intTable.Schema = item.Value
		case "RECNUM_TABLE":
			if strings.ToUpper(item.Value) == "YES" {
				intTable.Recnum = true
			}
		case "SYSTEM_FILE":
			if strings.ToUpper(item.Value) == "YES" {
				intTable.System = true
			}
		case "PRIMARY_INDEX":
			index, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "primary index must be a number")
			}
			intTable.PrimaryIndexNumber = index
		case "GENERATE_RECORD_ID_METHOD":
			intTable.RecordIdentity = item.Value
		case "TABLE_CHARACTER_FORMAT":
			intTable.Format = item.Value
		case "USE_DUMMY_ZERO_DATE":
			if strings.ToUpper(item.Value) == "YES" {
				intTable.UseDummyZeroDate = true
			}
		case "FIELD_NUMBER":
			if currentIndex != nil {
				// TODO: Validate index config
				intTable.Indexes = append(intTable.Indexes, *currentIndex)
				currentIndex = nil
			}
			fieldNum, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field number must be a number")
			}

			if currentField == nil {
				currentField = &df32.Field{
					Number: fieldNum,
				}
			}

			if currentField.Number != fieldNum {
				// TODO: Validate field config
				intTable.Fields = append(intTable.Fields, *currentField)
				currentField = &df32.Field{
					Number: fieldNum,
				}
			}
		case "FIELD_INDEX":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.Number)
			}
			index, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field index must be a number")
			}

			currentField.Index = index
		case "FIELD_LENGTH":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.Number)
			}
			length, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field index must be a number")
			}

			currentField.Length = length
		case "FIELD_PRECISION":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.Number)
			}
			precision, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field index must be a number")
			}

			currentField.Precision = precision
		case "FIELD_RELATED_FILE":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.Number)
			}
			fileNum, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field related file must be a number")
			}

			currentField.RelatesTo.File = fileNum
		case "FIELD_RELATED_FIELD":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.Number)
			}
			fieldNum, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field related field must be a number")
			}

			currentField.RelatesTo.Field = fieldNum
		case "INDEX_NUMBER":
			if currentField != nil {
				// TODO: Validate field config
				intTable.Fields = append(intTable.Fields, *currentField)
				currentField = nil
			}
			index, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "index number must be a number")
			}

			if currentIndex == nil {
				currentIndex = &df32.Index{
					Number: index,
				}
			}

			if currentIndex.Number != index {
				// TODO: Validate index config
				intTable.Indexes = append(intTable.Indexes, *currentIndex)
				currentIndex = &df32.Index{
					Number: index,
				}
			}

		case "INDEX_NAME":
			if currentIndex == nil {
				return intTable, errors.Errorf("index properties must come after field numbers. (%d)", item.Number)
			}

			currentIndex.Name = item.Value
		}
	}

	if currentField != nil {
		// TODO: Validate field config
		intTable.Fields = append(intTable.Fields, *currentField)
		currentField = nil
	}

	if currentIndex != nil {
		// TODO: Validate field config
		intTable.Indexes = append(intTable.Indexes, *currentIndex)
		currentIndex = nil
	}

	return intTable, nil
}
