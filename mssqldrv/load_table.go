package mssqldrv

import (
	"database/sql"
	"os"
	"strings"

	"gitlab.com/pureharvest/df32"

	"github.com/pkg/errors"
)

// Interfaces to to DB operations

type queryer interface {
	Query(query string, args ...interface{}) (*sql.Rows, error)
	QueryRow(query string, args ...interface{}) *sql.Row
}

type execer interface {
	Exec(query string, args ...interface{}) (*sql.Result, error)
}

type queryExecer interface {
	queryer
	execer
}

type tableLoader struct {
	db  queryer
	env *df32.Environment
}

func (driver tableLoader) LoadTableByName(name string) (df32.Table, error) {

	table := df32.Table{
		Name: name,
	}

	intPath := driver.env.SearchPath(strings.ToLower(name) + ".int")

	if intPath == "" {
		return table, errors.New("could not find intermediate file")
	}

	intFile, err := os.Open(intPath)

	if err != nil {
		return table, errors.Wrap(err, "could not open intermedaite file")
	}

	defer intFile.Close()

	intermediateFile, err := loadIntermediateFile(intFile)

	if err != nil {
		return table, errors.Wrap(err, "could not read intermedaite file")
	}

	return loadTableFromDB(intermediateFile, driver.db)

}

func loadTableFromDB(intFile Intermediate, db queryer) (df32.Table, error) {

	table := df32.Table{
		Name:   intFile.DatabaseName,
		System: intFile.System,
	}

	rows, err := db.Query(`
	SELECT ORDINAL_POSITION, COLUMN_NAME, DATA_TYPE, COLUMN_DEFAULT, NUMERIC_PRECISION, NUMERIC_SCALE, CHARACTER_MAXIMUM_LENGTH
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = ? AND TABLE_SCHEMA=?`, intFile.DatabaseName, intFile.Schema)

	if err != nil {
		return table, errors.Wrapf(err, "could not load table information: %#v", table.Name)
	}

	var fields []df32.Field

	for rows.Next() {
		var name, dataType, defaultValue sql.NullString
		var number, length int
		var precision, scale, maxLen sql.NullInt64
		if err := rows.Scan(&number, &name, &dataType, &defaultValue, &precision, &scale, &maxLen); err != nil {
			return table, errors.Wrap(err, "could not load table information")
		}

		if strings.EqualFold(name.String, "recnum") {
			continue
		}
		switch dataType.String {
		case "smallint":
			length = 4
		case "int":
			length = 6
		case "nchar":
			fallthrough
		case "char":
			fallthrough
		case "varchar":
			fallthrough
		case "nvarchar":
			length = int(maxLen.Int64)
		case "decimal":
			fallthrough
		case "numeric":
			length = int(precision.Int64)
		}

		ft, err := df32.ConvertSQLTypeToDataflex(dataType.String)

		if err != nil {
			return table, errors.Wrapf(err, "unknown type in %s", dataType.String)
		}

		if !intFile.System {
			number--
		}

		field := df32.Field{
			Number:    number,
			Name:      name.String,
			Type:      ft,
			Length:    length,
			Precision: int(scale.Int64),
		}

		if field.Type == df32.DateType {
			field.Length = 6
			field.Precision = 0
		}

		fields = append(fields, field)
	}

	for _, field := range intFile.Fields {

		if field.Number > len(fields) || field.Number == 0 {
			return table, errors.Errorf("intermediate file refers to a field %d that doesn't exist", field.Number)
		}

		if field.Index != 0 {
			fields[field.Number-1].Index = field.Index
		}

		if field.RelatesTo.File != 0 {
			fields[field.Number-1].RelatesTo = field.RelatesTo
		}

		if field.Length != 0 {
			fields[field.Number-1].Length = field.Length
			fields[field.Number-1].Precision = field.Precision
		}
	}

	table.Fields = fields

	rows, err = db.Query(`
SELECT  ind.name AS IndexName
      , ind.is_primary_key AS IsPrimaryKey
      , ind.is_unique AS IsUniqueIndex
      , col.name AS ColumnName
      , ic.is_included_column AS IsIncludedColumn
      , ic.key_ordinal AS ColumnOrder
	  , ic.is_descending_key As IsDescending
FROM    sys.indexes ind
        INNER JOIN sys.index_columns ic
            ON ind.object_id = ic.object_id
               AND ind.index_id = ic.index_id
        INNER JOIN sys.columns col
            ON ic.object_id = col.object_id
               AND ic.column_id = col.column_id
        INNER JOIN sys.tables t
            ON ind.object_id = t.object_id
WHERE   t.is_ms_shipped = 0
	AND OBJECT_SCHEMA_NAME(ind.object_id) = ? AND OBJECT_NAME(ind.object_id) = ?
ORDER BY ind.is_primary_key DESC
      , ind.is_unique DESC
      , ind.name --IndexName
      , ic.key_ordinal
`, intFile.Schema, intFile.DatabaseName)

	if err != nil {
		return table, errors.Wrap(err, "could not read indexes")
	}

	indexes := map[string]df32.Index{}

	for rows.Next() {
		var indexName, columnName string
		var isIncluded, isDescending, isPrimary, isUnique bool
		var columnNumber int

		if err := rows.Scan(&indexName, &isPrimary, &isUnique, &columnName, &isIncluded, &columnNumber, &isDescending); err != nil {
			return table, errors.Wrap(err, "could not load table information")
		}

		currentIndex, found := indexes[indexName]

		if !found {
			currentIndex.Name = indexName
			currentIndex.PrimaryKey = isPrimary
		}

		if !isIncluded {
			currentIndex.Fields = append(currentIndex.Fields, df32.IndexField{
				Name:       columnName,
				Number:     columnNumber,
				Descending: isDescending,
			})
		}

		indexes[indexName] = currentIndex

	}

	if rows.Err() != nil {
		return table, errors.Wrap(rows.Err(), "could not read indexes")
	}

	for _, index := range intFile.Indexes {
		if _, found := indexes[index.Name]; found {

			index.PrimaryKey = indexes[index.Name].PrimaryKey
			index.Fields = indexes[index.Name].Fields

			table.Indexes = append(table.Indexes, index)
		}
	}

	return table, nil
}
