package mssqldrv

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log/slog"
	neturl "net/url"

	"gitlab.com/pureharvest/df32"
	"gitlab.com/pureharvest/dfmigrate"

	"strings"

	"os"

	"github.com/pkg/errors"

	"path/filepath"

	_ "github.com/microsoft/go-mssqldb"
)

type versionInfo uint64

const tableName = "schema_migrations"
const driverName = "mssqldrv"

type Driver struct {
	intPath         string
	outputPath      string
	schema          string
	migrationsTable string
	url             string
	filelistPath    string
	applyOnly       bool

	generator SQLGenerator

	filelist df32.Filelist
	db       *sql.DB
	log      *slog.Logger
}

type fileOp struct {
	operation string
	filename  string
	data      []byte
}

// Initialize will connect with mssqldrv://user:pass
func (driver *Driver) Initialize(cfg dfmigrate.Options) error {
	var err error

	driver.intPath = cfg.IntermediatePath

	driver.url = cfg.URL

	if !strings.Contains(driver.url, "://") {
		driver.url = "sqlserver://" + driver.url
	}

	driver.migrationsTable = cfg.MigrationsTable
	driver.filelistPath = cfg.FilelistPath

	if driver.migrationsTable == "" {
		driver.migrationsTable = tableName
	}

	driver.generator = configureGenerator(cfg)

	if driver.filelistPath != "" {

		driver.filelist, err = df32.Open(driver.filelistPath)

		if err != nil {
			return errors.Wrap(err, "could not read filelist")
		}
	} else {
		driver.filelistPath = "filelist.cfg"
		driver.filelist, _ = df32.NewFilelist(driver.filelistPath)
	}

	if driver.db != nil {
		driver.db.Close()
	}

	driver.db, err = sql.Open("mssql", driver.url)

	if err != nil {
		return errors.Wrap(err, "could not connect to database")
	}

	if err := driver.db.Ping(); err != nil {
		return errors.Wrap(err, "could not connect to database")
	}

	if err := ensureSchemaExists(driver.db, driver.generator.schema); err != nil {
		return err
	}

	if err := ensureVersionTableExists(driver.db); err != nil {
		return err
	}

	return nil

}

func (driver *Driver) SetLogger(logger *slog.Logger) {
	driver.log = logger
}

func (driver *Driver) Bootstrap(url string) error {

	if !strings.Contains(url, "://") {
		url = "sqlserver://" + url
	}

	parsedURL, err := neturl.Parse(url)

	if err != nil {
		return err
	}

	query := parsedURL.Query()
	database := query.Get("database")
	query.Set("database", "master")

	parsedURL.RawQuery = query.Encode()
	driver.log.Debug(parsedURL.String())

	db, err := sql.Open("sqlserver", parsedURL.String())

	if err != nil {
		return err
	}

	if err := db.Ping(); err != nil {
		return err
	}

	if _, err := db.Exec(fmt.Sprintf("CREATE DATABASE [%s] COLLATE Latin1_General_BIN2", database)); err != nil {
		return err
	}

	if _, err := db.Exec(fmt.Sprintf("ALTER DATABASE [%s] SET ALLOW_SNAPSHOT_ISOLATION ON", database)); err != nil {
		return err
	}

	return db.Close()
}

func (driver *Driver) GenerateFiles(options dfmigrate.Options, model dfmigrate.Model) error {

	schema := options.ExtraOptions["schema"]

	if schema == "" {
		schema = "dataflex"
	}

	for _, table := range model.Tables() {
		if err := writeIntermediateFile(
			CreateIntermediate(schema, table),
			filepath.Join(options.IntermediatePath, strings.ToLower(table.Name)+".int"),
		); err != nil {
			return errors.Wrapf(err, "couldn't write intermediate file for %s", table.Name)
		}

	}

	return nil
}

func (driver *Driver) Close() error {
	if driver.db != nil {
		driver.db.Close()
	}
	return nil
}

func (driver *Driver) Model() (dfmigrate.Model, error) {
	return driver.loadModel(driver.db)
}

func (driver *Driver) loadModel(db queryer) (dfmigrate.Model, error) {
	var err error
	var version uint64

	if version, err = driver.version(db); err != nil {
		return dfmigrate.Model{}, err
	}

	model := dfmigrate.NewModel(version)

	for _, entry := range driver.filelist.GetAll() {

		if strings.EqualFold(entry.Driver, "DATAFLEX") {
			driver.log.Debug(fmt.Sprintf("%d:%s is a native table. ignoring...", entry.Number, entry.Name))
			continue
		}

		driver.log.Debug(fmt.Sprintf("loading table %s:%d:%s", entry.Driver, entry.Number, entry.Name))

		table, err := driver.loadTable(db, entry.Name)

		if err != nil {
			return model, errors.Wrapf(err, "could not load table: %s", entry.Name)
		}

		table.Number = entry.Number
		table.DisplayName = entry.DisplayName

		model.Set(int(entry.Number), table)
	}

	driver.log.Debug("model loaded...")

	return model, nil
}

func (driver *Driver) loadTable(db queryer, name string) (df32.Table, error) {
	intPath := filepath.Join(driver.intPath, strings.ToLower(name+".int"))

	intFile, err := os.Open(intPath)

	if err != nil {
		return df32.Table{}, err
	}
	defer intFile.Close()

	intermediateFile, err := loadIntermediateFile(intFile)

	if err != nil {
		return df32.Table{}, errors.Wrap(err, "could not read intermedaite file")
	}

	return loadTableFromDB(intermediateFile, db)

}

func (driver *Driver) Apply(file dfmigrate.Migration) error {

	driver.log.Debug("Running migation", file.Name, file.Direction)

	tx, err := driver.db.Begin()

	if err != nil {
		return errors.Wrap(err, "could not run migration")
	}

	if file.Direction == dfmigrate.Up {
		if _, err := tx.Exec("INSERT INTO [dbo].["+driver.migrationsTable+"] (version) VALUES ($1)", file.Version); err != nil {
			return driver.rollBackOnError(err, tx)
		}
	} else if file.Direction == dfmigrate.Down {
		if _, err := tx.Exec("DELETE FROM [dbo].["+driver.migrationsTable+"] WHERE version=$1", file.Version); err != nil {
			return driver.rollBackOnError(err, tx)
		}
	}

	var statements []string

	if len(file.Tables) > 0 {

		// Prepare migrations
		driver.log.Debug("prepare dfmigrate...")
		preparedMigrations := make([]dfmigrate.TableMigration, len(file.Tables))

		for i, migration := range file.Tables {
			driver.log.Debug(fmt.Sprintf("preparing %s...", migration.Name))
			migration, err := dfmigrate.PrepareTableMigration(migration)
			if err != nil {
				return driver.rollBackOnError(errors.Wrap(err, "unable to prepare migrations"), tx)
			}
			preparedMigrations[i] = migration
		}

		for _, migration := range file.Tables {
			driver.log.Debug(fmt.Sprintf("building sql for %s", migration.Name))

			s, err := GenerateTableMigrationScript(migration, driver.generator)

			if err != nil {
				return driver.rollBackOnError(err, tx)
			}

			statements = append(statements, s...)

		}

		driver.log.Debug("running sql...")

		for _, statement := range statements {
			driver.log.Debug(statement)
			if _, err := tx.Exec(statement); err != nil {

				return driver.rollBackOnError(err, tx)
			}

		}

	} else if len(file.SQL) != 0 {

		for _, statement := range file.SQL {
			driver.log.Debug(statement)

			_, err := tx.Exec(statement)

			if err != nil {
				return driver.rollBackOnError(errors.Wrapf(err, "could not commit %s", statement), tx)
			}
		}

	}

	if err := tx.Commit(); err != nil {
		return driver.rollBackOnError(errors.Wrap(err, "commit error"), tx)
	}

	driver.log.Debug("Finished migration...")
	return driver.filelist.Save()
}

func (driver *Driver) generateFiles(preparedMigrations []dfmigrate.TableMigration, tables map[string]df32.Table) error {

	// Create a temp directory to store changed files
	tmpDir, err := ioutil.TempDir(os.TempDir(), "mssqldrv")

	if err != nil {
		return err
	}

	defer os.RemoveAll(tmpDir)

	var fileOperations []fileOp

	for _, migration := range preparedMigrations {

		filename := strings.ToLower(migration.Name + ".int")
		table, _ := tables[migration.Name]

		if migration.Action == dfmigrate.CreateAction || migration.Action == dfmigrate.AlterAction {

			// Assume it exists if we got to this point

			table, err = dfmigrate.ApplyMigration(table, migration)

			if err != nil {
				return err
			}

			entry := df32.FilelistEntry{
				Number:       table.Number,
				DisplayName:  table.DisplayName,
				Name:         table.Name,
				DataflexName: table.Name,
				Driver:       "OTHER",
			}

			table.Indexes = createMissingIndexes(table)

			intermediateFile := CreateIntermediate(driver.generator.schema, table)

			data, err := intermediateFile.MarshalText()

			if err != nil {
				driver.log.Debug("couldn't generate intermediate file")
				return err
			}

			fileOperations = append(fileOperations, fileOp{
				operation: "add",
				data:      data,
				filename:  filename,
			})

			if err := driver.filelist.Set(entry); err != nil {
				driver.log.Debug("couldn't update filelist")
				return err
			}

		}

		if migration.Action == dfmigrate.DeleteAction {

			fileOperations = append(fileOperations, fileOp{
				operation: "delete",
				filename:  filename,
			})

			if err := driver.filelist.Delete(int(table.Number)); err != nil {
				driver.log.Debug("couldn't update filelist")
				return err
			}
		}
	}

	for _, op := range fileOperations {

		targetFilename := getTargetFilePath(driver, op)

		switch op.operation {
		case "add":
			driver.log.Debug(fmt.Sprintf("creating %s", targetFilename))
			if targetFilename == "" {
				targetFilename = filepath.Join(".", op.filename)
			}

			err := ioutil.WriteFile(targetFilename, op.data, 0666)
			if err != nil {
				return err
			}
		case "delete":
			if targetFilename != "" {
				driver.log.Debug(fmt.Sprintf("deleting %s", targetFilename))
				if err := os.Remove(targetFilename); err != nil {
					return err
				}
			} else {
				return errors.Errorf("could not find file %s", op.filename)
			}
		}
	}

	return nil
}

func getTargetFilePath(driver *Driver, op fileOp) string {

	var prefix string

	if strings.Contains(op.filename, ".int") && driver.intPath != "" {
		prefix = driver.intPath
	} else {
		prefix = "."
	}

	return filepath.Join(prefix, op.filename)

}

func (driver *Driver) rollBackOnError(err error, tx *sql.Tx) error {
	if err2 := tx.Rollback(); err2 != nil {
		return errors.Wrap(err, err2.Error())
	}
	return err
}

func (driver *Driver) Version() (uint64, error) {
	return driver.version(driver.db)
}

func (driver *Driver) version(db queryer) (uint64, error) {
	var version uint64
	err := db.QueryRow("SELECT TOP 1 version FROM [dbo].[" + driver.migrationsTable + "] ORDER BY version DESC").Scan(&version)
	switch {
	case err == sql.ErrNoRows:
		return 0, nil
	case err != nil:
		return 0, err
	default:
		return version, nil
	}
}

func (driver *Driver) Baseline(version uint64) error {

	tx, err := driver.db.Begin()

	if err != nil {
		return errors.Wrap(err, "could not set baseline")
	}

	if _, err := tx.Exec("INSERT INTO [dbo].["+driver.migrationsTable+"] (version) VALUES ($1)", version); err != nil {
		return driver.rollBackOnError(err, tx)
	}

	if err := tx.Commit(); err != nil {
		return driver.rollBackOnError(err, tx)
	}

	return nil
}
