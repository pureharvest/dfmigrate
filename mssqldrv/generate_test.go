package mssqldrv

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pureharvest/df32"
	"gitlab.com/pureharvest/dfmigrate"
)

func Test_CreateTableMigrationScript(t *testing.T) {
	tests := []struct {
		name      string
		migration dfmigrate.TableMigration
		expected  []string
	}{
		{
			name: "provided clustered index",
			migration: dfmigrate.TableMigration{
				Action: "create",
				Name:   "TEST",
				Fields: []dfmigrate.FieldMigration{
					dfmigrate.FieldMigration{Name: "FIELD1", Type: df32.AsciiType, Length: 10},
				},
				Indexes: []dfmigrate.IndexMigration{
					dfmigrate.IndexMigration{Name: "TEST001_PK", PrimaryKey: true, Fields: []dfmigrate.IndexField{{Name: "FIELD1", Descending: true}}},
				},
			},
			expected: []string{
				`CREATE TABLE [test].[TEST] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [FIELD1] NVARCHAR(10) CONSTRAINT [DF__TEST__FIELD1] DEFAULT ('') NOT NULL
    CONSTRAINT [TEST001_PK] PRIMARY KEY CLUSTERED ([FIELD1] DESC)
)`,
				"CREATE UNIQUE NONCLUSTERED INDEX [TEST000] ON [test].[TEST] ([RECNUM] ASC)",
			},
		},
		{
			name: "defaults to cluster on recnum",
			migration: dfmigrate.TableMigration{
				Action: "create",
				Name:   "TEST",
				Fields: []dfmigrate.FieldMigration{
					dfmigrate.FieldMigration{Name: "FIELD1", Type: "ascii", Length: 10},
				},
				Indexes: []dfmigrate.IndexMigration{
					dfmigrate.IndexMigration{Name: "TEST001", Fields: []dfmigrate.IndexField{{Name: "FIELD1", Descending: true}}},
				},
			},
			expected: []string{
				`CREATE TABLE [test].[TEST] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [FIELD1] NVARCHAR(10) CONSTRAINT [DF__TEST__FIELD1] DEFAULT ('') NOT NULL
    CONSTRAINT [TEST000_PK] PRIMARY KEY CLUSTERED ([RECNUM] ASC)
)`,
				"CREATE UNIQUE NONCLUSTERED INDEX [TEST001] ON [test].[TEST] ([FIELD1] DESC)",
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			generator := SQLGenerator{
				schema: "test",
			}

			statements, err := GenerateTableMigrationScript(test.migration, generator)

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected,
				statements,
			)
		})
	}
}

func Test_AlterTableMigrationScript(t *testing.T) {
	tests := []struct {
		name      string
		migration dfmigrate.TableMigration
		expected  []string
	}{
		{
			name: "single column rename",
			migration: dfmigrate.TableMigration{
				Action: "alter",
				Name:   "TEST",
				Fields: []dfmigrate.FieldMigration{
					dfmigrate.FieldMigration{
						Action:  "alter",
						Name:    "BEFORE",
						NewName: "AFTER",
					},
				},
			},
			expected: []string{
				`EXEC sp_rename @objname=N'[test].[TEST].[BEFORE]', @newname=N'AFTER', @objtype=N'COLUMN';`,
			},
		},
		{
			name: "add columns",
			migration: dfmigrate.TableMigration{
				Action: "alter",
				Name:   "TEST",
				Fields: []dfmigrate.FieldMigration{
					{
						Action: "create",
						Name:   "NEWFIELD",
						Type:   "ascii",
						Length: 10,
					},
				},
			},
			expected: []string{
				`ALTER TABLE [test].[TEST] ADD [NEWFIELD] NVARCHAR(10) CONSTRAINT [DF__TEST__NEWFIELD] DEFAULT ('') NOT NULL;`,
			},
		},
		{
			name: "drop columns",
			migration: dfmigrate.TableMigration{
				Action: "alter",
				Name:   "TEST",
				Fields: []dfmigrate.FieldMigration{
					{
						Action: "delete",
						Name:   "OLDFIELD",
					},
				},
			},
			expected: []string{
				`ALTER TABLE [test].[TEST] DROP CONSTRAINT [DF__TEST__OLDFIELD];`,
				`ALTER TABLE [test].[TEST] DROP COLUMN [OLDFIELD];`,
			},
		},
		{
			name: "add, alter and drop columns",
			migration: dfmigrate.TableMigration{
				Action: "alter",
				Name:   "TEST",
				Fields: []dfmigrate.FieldMigration{
					{
						Action: "create",
						Name:   "ADD_FIELD",
						Type:   "ascii",
						Length: 10,
					},
					{
						Action:  "alter",
						Name:    "BEFORE",
						NewName: "AFTER",
					},
					{
						Action: "delete",
						Name:   "DROP_FIELD",
					},
				},
			},
			expected: []string{
				`ALTER TABLE [test].[TEST] ADD [ADD_FIELD] NVARCHAR(10) CONSTRAINT [DF__TEST__ADD_FIELD] DEFAULT ('') NOT NULL;`,
				`EXEC sp_rename @objname=N'[test].[TEST].[BEFORE]', @newname=N'AFTER', @objtype=N'COLUMN';`,
				`ALTER TABLE [test].[TEST] DROP CONSTRAINT [DF__TEST__DROP_FIELD];`,
				`ALTER TABLE [test].[TEST] DROP COLUMN [DROP_FIELD];`,
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			generator := SQLGenerator{
				schema: "test",
			}
			statements, err := GenerateTableMigrationScript(test.migration, generator)

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected,
				statements,
			)
		})
	}
}

func Test_IndexValidation(t *testing.T) {
	tests := []struct {
		name string
		in   dfmigrate.TableMigration
		out  []df32.Index
	}{
		{
			name: "with recnum index",
			in: dfmigrate.TableMigration{
				Indexes: []dfmigrate.IndexMigration{
					dfmigrate.IndexMigration{
						Name: "test",
						Fields: []dfmigrate.IndexField{
							{Name: "RECNUM"},
						},
					},
				},
			},
			out: []df32.Index{
				{
					Number: 0,
					Name:   "test",
					Fields: []df32.IndexField{
						{Name: "RECNUM"},
					},
				},
			},
		},
		{
			name: "without index",
			in: dfmigrate.TableMigration{
				Name: "TEST",
			},
			out: []df32.Index{
				{
					Name:       "TEST000_PK",
					PrimaryKey: true,
					Fields: []df32.IndexField{
						{Name: "RECNUM"},
					},
				},
			},
		},
		{
			name: "with index, but no recnum",
			in: dfmigrate.TableMigration{
				Name: "TEST",
				Indexes: []dfmigrate.IndexMigration{
					{
						Name: "TEST001",
						Fields: []dfmigrate.IndexField{
							{Name: "FIELD"},
						},
					},
				},
			},

			out: []df32.Index{
				{
					Name:       "TEST000_PK",
					PrimaryKey: true,
					Fields: []df32.IndexField{
						{Name: "RECNUM"},
					},
				},
				{
					Number: 1,
					Name:   "TEST001",
					Fields: []df32.IndexField{
						{Name: "FIELD"},
					},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			indexes := createMissingIndexes(test.in.Table())

			assert.Equal(t, test.out, indexes)
		})
	}
}
