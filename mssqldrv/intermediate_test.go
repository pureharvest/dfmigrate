package mssqldrv

import (
	"bytes"
	"testing"

	"github.com/andybalholm/crlf"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pureharvest/df32"
	"golang.org/x/text/transform"
)

func Test_IntermediateRead(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected Intermediate
		err      error
	}{
		{
			input: `
DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME BLINE
SCHEMA_NAME dataflex

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES

INDEX_NUMBER 0
INDEX_NAME TEST000_PK
`,
			expected: Intermediate{
				DatabaseName:     "BLINE",
				Schema:           "dataflex",
				RecordIdentity:   "IDENTITY_COLUMN",
				UseDummyZeroDate: true,
				Format:           "ANSI",
				System:           false,
				Recnum:           true,
				Server:           "DFCONNID=POSONA;",
				Indexes: []df32.Index{
					df32.Index{Number: 0, Name: "TEST000_PK"},
				},
			},
		},
		{
			input: `
DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME BLINE
SCHEMA_NAME dataflex

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES

FIELD_NUMBER 1
FIELD_INDEX 1

FIELD_NUMBER 6
FIELD_LENGTH 5
FIELD_RELATED_FILE 96
FIELD_RELATED_FIELD 1


INDEX_NUMBER 0
INDEX_NAME USEROP000

INDEX_NUMBER 1
INDEX_NAME USEROP001

`,
			expected: Intermediate{
				DatabaseName:     "BLINE",
				Schema:           "dataflex",
				RecordIdentity:   "IDENTITY_COLUMN",
				UseDummyZeroDate: true,
				Format:           "ANSI",
				System:           false,
				Recnum:           true,
				Server:           "DFCONNID=POSONA;",
				Fields: []df32.Field{
					df32.Field{Number: 1, Index: 1},
					df32.Field{Number: 6, Length: 5, RelatesTo: df32.FieldRelation{File: 96, Field: 1}},
				},
				Indexes: []df32.Index{
					df32.Index{Number: 0, Name: "USEROP000"},
					df32.Index{Number: 1, Name: "USEROP001"},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			file, err := loadIntermediateFile(bytes.NewBufferString(test.input))

			assert.Equal(t, test.err, err)
			assert.Equal(t, test.expected, file)
		})
	}
}

func Test_IntermediateWrite(t *testing.T) {
	tests := []struct {
		name     string
		expected string
		input    Intermediate
		err      error
	}{
		{
			expected: `DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME BLINE
SCHEMA_NAME dataflex

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES

INDEX_NUMBER 0
INDEX_NAME TEST000_PK
`,
			input: Intermediate{
				DatabaseName:     "BLINE",
				Schema:           "dataflex",
				RecordIdentity:   "IDENTITY_COLUMN",
				UseDummyZeroDate: true,
				Format:           "ANSI",
				System:           false,
				Recnum:           true,
				Server:           "DFCONNID=POSONA;",
				Indexes: []df32.Index{
					df32.Index{Number: 0, Name: "TEST000_PK"},
				},
			},
		},
		{
			expected: `DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME BLINE
SCHEMA_NAME dataflex

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES

FIELD_NUMBER 1
FIELD_INDEX 1

FIELD_NUMBER 6
FIELD_RELATED_FILE 96
FIELD_RELATED_FIELD 1

INDEX_NUMBER 0
INDEX_NAME USEROP000

INDEX_NUMBER 1
INDEX_NAME USEROP001
`,
			input: Intermediate{
				DatabaseName:     "BLINE",
				Schema:           "dataflex",
				RecordIdentity:   "IDENTITY_COLUMN",
				UseDummyZeroDate: true,
				Format:           "ANSI",
				System:           false,
				Recnum:           true,
				Server:           "DFCONNID=POSONA;",
				Fields: []df32.Field{
					df32.Field{Number: 1, Index: 1},
					df32.Field{Number: 6, Length: 5, RelatesTo: df32.FieldRelation{File: 96, Field: 1}},
				},
				Indexes: []df32.Index{
					df32.Index{Number: 0, Name: "USEROP000"},
					df32.Index{Number: 1, Name: "USEROP001"},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			expected, _, _ := transform.String(crlf.ToCRLF{}, test.expected)

			output, err := test.input.MarshalText()

			assert.Equal(t, test.err, err)
			assert.Equal(t, expected, string(output))
		})
	}
}
