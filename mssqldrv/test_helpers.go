package mssqldrv

import (
	"database/sql"
	"fmt"
	"math/rand"
	"os"
	"testing"
	"time"
)

func createDB(t *testing.T, db *sql.DB, name string) {

	t.Log("Creating Database ", name)
	if _, err := db.Exec(fmt.Sprintf("CREATE DATABASE %s", name)); err != nil {
		t.Fatal(err)
	}

}

func dropDB(t *testing.T, db *sql.DB, name string) {
	t.Log("Dropping Database ", name)
	// if _, err := db.Exec(fmt.Sprintf("ALTER DATABASE [%s] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;", name)); err != nil {
	// 	t.Fatal(err)
	// }

	if _, err := db.Exec(fmt.Sprintf("DROP DATABASE %s", name)); err != nil {
		t.Fatal(err)
	}
}

func randomDBName(prefix string) string {

	rand.Seed(time.Now().UTC().UnixNano())
	const chars = "abcdefghijklmnopqrstuvwxyz0123456789"
	result := make([]byte, 10)
	for i := 0; i < 10; i++ {
		result[i] = chars[rand.Intn(len(chars))]
	}
	return prefix + string(result)

}

type TestDB struct {
	Name             string
	Host             string
	Username         string
	Password         string
	ConnectionString string
	*sql.DB
}

func CreateTestMSSQLDRVEnvironment(t *testing.T, schema string) (TestDB, func()) {

	host := os.Getenv("MSSQL_PORT_1433_TCP_ADDR")
	user := os.Getenv("MSSQL_USER")
	password := os.Getenv("MSSQL_PASSWORD")

	if host == "" {
		host = "127.0.0.1"
	}

	databaseName := randomDBName("df")

	var connectionString string

	if user == "" || password == "" {
		connectionString = fmt.Sprintf("%s", host)
	} else {
		connectionString = fmt.Sprintf("%s:%s@%s", user, password, host)
	}

	db, err := sql.Open("sqlserver", "sqlserver://"+connectionString+"?database=master&TrustServerCertificate=true")

	if err != nil {
		t.Fatalf("%#v", err)
	}

	if err := db.Ping(); err != nil {
		t.Fatalf("%#v", err)
	}

	createDB(t, db, databaseName)

	testDB := TestDB{
		Name:             databaseName,
		Host:             host,
		Username:         user,
		Password:         password,
		ConnectionString: connectionString + "?TrustServerCertificate=true&database=" + databaseName,
	}

	// Create separate connection to support Azure sql
	testDB.DB, err = sql.Open("sqlserver", "sqlserver://"+testDB.ConnectionString)

	if err != nil {
		t.Fatal(err)
	}

	_, err = testDB.Exec(fmt.Sprintf("CREATE SCHEMA %s;", schema))

	if err != nil {
		t.Fatal(err)
	}

	cleanUp := func() {
		t.Log("cleaning up database...")
		testDB.Close()
		dropDB(t, db, databaseName)
		db.Close()
	}

	return testDB, cleanUp

}
