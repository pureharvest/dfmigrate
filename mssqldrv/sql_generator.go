package mssqldrv

import (
	"fmt"
	"strings"

	"gitlab.com/pureharvest/df32"
	"gitlab.com/pureharvest/dfmigrate"
)

type SQLGenerator struct {
	schema   string
	types    df32.TypeMap
	defaults map[df32.Type]string
}

var defaultTypeMap = df32.TypeMap{
	df32.AsciiType:  "NVARCHAR",
	df32.NumberType: "NUMERIC",
	df32.DateType:   "DATE",
	df32.TextType:   "NTEXT",
	df32.BinaryType: "BINARY",
	//Non-default for legacy
	"char":     "CHAR",
	"datetime": "DATETIME",
}

var defaultValues = map[df32.Type]string{
	df32.AsciiType:  "''",
	df32.NumberType: "0",
	df32.DateType:   "'0001-01-01'",
	df32.TextType:   "''",
	df32.BinaryType: "0",
	"char":          "''",
	"datetime":      "'0001-01-01'",
}

func sqlLength(f df32.Field) string {
	if f.Type == "date" || f.Type == "text" || f.Type == "datetime" {
		return ""
	}

	if strings.EqualFold(f.Name, "recnum") {
		return " (1, 1)"
	}

	if f.Type == "number" {
		if f.Precision == 0 {
			return fmt.Sprintf("(%d)", f.Length)
		}

		return fmt.Sprintf("(%d,%d)", f.Length, f.Precision)
	}

	return fmt.Sprintf("(%d)", f.Length)
}

func fieldType(field df32.Field, types df32.TypeMap) string {

	if strings.EqualFold(field.Name, "recnum") {
		return "INT IDENTITY"
	}

	if field.Type == df32.NumberType && field.Precision == 0 && field.Length <= 4 {
		return "SMALLINT"
	}

	if field.Type == df32.NumberType && field.Precision == 0 && field.Length <= 6 {
		return "INT"
	}

	if types == nil {
		types = defaultTypeMap
	}

	return strings.ToUpper(types[field.Type]) + sqlLength(field)
}

func fieldDefault(field df32.Field, defaults map[df32.Type]string) string {
	if defaults == nil {
		defaults = defaultValues
	}

	return defaults[field.Type]
}

func configureGenerator(options dfmigrate.Options) SQLGenerator {
	generator := SQLGenerator{
		schema:   "dataflex",
		types:    defaultTypeMap,
		defaults: defaultValues,
	}

	if schema := options.ExtraOptions["schema"]; schema != "" {
		generator.schema = schema
	}

	if dateType := options.ExtraOptions["date_type"]; dateType != "" {
		generator.types[df32.DateType] = strings.ToUpper(dateType)
	}

	if numberType := options.ExtraOptions["number_type"]; numberType != "" {
		generator.types[df32.NumberType] = strings.ToUpper(numberType)
	}

	if asciiType := options.ExtraOptions["ascii_type"]; asciiType != "" {
		generator.types[df32.AsciiType] = strings.ToUpper(asciiType)
	}

	if dateDefault := options.ExtraOptions["date_default"]; dateDefault != "" {
		generator.defaults[df32.DateType] = dateDefault
	}

	if numberDefault := options.ExtraOptions["number_default"]; numberDefault != "" {
		generator.defaults[df32.NumberType] = numberDefault
	}

	if asciiDefault := options.ExtraOptions["ascii_default"]; asciiDefault != "" {
		generator.defaults[df32.AsciiType] = asciiDefault
	}

	return generator
}

func (g SQLGenerator) fieldDefinition(table string, field df32.Field) string {

	fieldType := fieldType(field, g.types)
	fieldDefault := fieldDefault(field, g.defaults)

	// Special case for identity fields
	if strings.EqualFold(field.Name, "recnum") {
		return fmt.Sprintf("[%s] %s (1, 1) NOT NULL", field.Name, fieldType)
	}

	return fmt.Sprintf(
		"[%s] %s CONSTRAINT [DF__%s__%s] DEFAULT (%s) NOT NULL",
		field.Name,
		fieldType,
		table,
		field.Name,
		fieldDefault,
	)
}

func indexFieldsDefinition(fields []df32.IndexField) string {
	fieldDefs := make([]string, 0, len(fields))

	for _, field := range fields {
		direction := "ASC"

		if field.Descending {
			direction = "DESC"
		}

		fieldDefs = append(fieldDefs, fmt.Sprintf("[%s] %s", field.Name, direction))
	}

	return strings.Join(fieldDefs, ",")
}

func clusteredIndex(table string, indexes []df32.Index) string {
	var name string
	var fields []df32.IndexField

	for _, index := range indexes {
		if index.PrimaryKey {
			name = index.Name
			fields = index.Fields
			break
		}
	}

	if name == "" {
		return ""
	}

	return fmt.Sprintf("CONSTRAINT [%s] PRIMARY KEY CLUSTERED (%s)", name, indexFieldsDefinition(fields))
}

func (g SQLGenerator) CreateIndex(schema, table, name string, isClustered bool, fields []df32.IndexField) string {
	clustered := "NONCLUSTERED"

	if isClustered {
		clustered = "CLUSTERED"
	}

	return fmt.Sprintf(`CREATE UNIQUE %s INDEX [%s] ON [%s].[%s] (%s)`, clustered, name, schema, table, indexFieldsDefinition(fields))

}

func (g SQLGenerator) CreateTable(table df32.Table) string {
	name := table.Name
	fields := table.Fields
	schema := g.schema
	indexes := table.Indexes

	fieldDefs := make([]string, 0, len(fields)+1)

	if !table.System && len(fields) > 0 && fields[0].Name != "RECNUM" {
		fieldDefs = append(fieldDefs, g.fieldDefinition(name, df32.Field{
			Name: "RECNUM",
		}))
	}

	for _, field := range fields {
		fieldDefs = append(fieldDefs, g.fieldDefinition(name, field))
	}

	clusteredIndex := clusteredIndex(name, indexes)

	return fmt.Sprintf(`CREATE TABLE [%s].[%s] (
    %s
    %s
)`, schema, name, strings.Join(fieldDefs, ",\n    "), clusteredIndex)
}

func (g SQLGenerator) DropTable(table string) string {
	return fmt.Sprintf(`DROP TABLE [%s].[%s];`, g.schema, table)
}

func (g SQLGenerator) AddField(schema, table string, field df32.Field) string {
	return fmt.Sprintf(`ALTER TABLE [%s].[%s] ADD %s;`, schema, table, g.fieldDefinition(table, field))
}

func (g SQLGenerator) AlterField(schema, table, from string, to df32.Field) string {

	return fmt.Sprintf(`ALTER TABLE [%s].[%s] ALTER COLUMN [%s] %s;`, schema, table, from, fieldType(to, g.types))
}

func (g SQLGenerator) DropField(schema, table, field string) string {
	return fmt.Sprintf(`ALTER TABLE [%s].[%s] DROP COLUMN [%s];`, schema, table, field)
}

func (g SQLGenerator) DropFieldDefault(schema, table, field string) string {
	return fmt.Sprintf(`ALTER TABLE [%s].[%s] DROP CONSTRAINT [DF__%s__%s];`, schema, table, table, field)
}

func (g SQLGenerator) RenameField(schema, table, from, to string) string {
	return fmt.Sprintf(`EXEC sp_rename @objname=N'[%s].[%s].[%s]', @newname=N'%s', @objtype=N'COLUMN';`, schema, table, from, to)
}
