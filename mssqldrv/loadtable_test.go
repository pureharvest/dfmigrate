package mssqldrv

import "testing"

func Test_LoadTable(t *testing.T) {

	testDB, cleanup := CreateTestMSSQLDRVEnvironment(t, "dataflex")

	defer cleanup()
	defer testDB.Close()

	_, err := testDB.Exec(`CREATE TABLE [dataflex].[BLINE] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [NUMBER] NVARCHAR (10) CONSTRAINT [DF__TEST__NUMBER] DEFAULT ('') NOT NULL
    CONSTRAINT [TEST000_PK] PRIMARY KEY CLUSTERED ([RECNUM] ASC)
);`)

	if err != nil {
		t.Fatal(err)
	}

    
}
