package mssqldrv

import (
	"testing"

	neturl "net/url"

	"github.com/bmizerany/assert"
)

func Test_ParseConnectionString(t *testing.T) {
	tests := []struct {
		url              string
		options          neturl.Values
		connectionString string
		err              error
	}{
		{
			url:              "mssqldrv://example.com?test=1",
			options:          neturl.Values{},
			connectionString: "sqlserver://example.com?test=1",
			err:              nil,
		},
		{
			url: "mssqldrv://user:password@example.com?database=test&dfpath=/test",
			options: neturl.Values{
				"dfpath": []string{"/test"},
			},
			connectionString: "sqlserver://user:password@example.com?database=test",
			err:              nil,
		},
	}

	for _, test := range tests {
		t.Run(test.url, func(t *testing.T) {
			connectionString, options, err := parseConnectionString(test.url)

			assert.Equal(t, test.connectionString, connectionString)
			assert.Equal(t, test.options, options)
			assert.Equal(t, test.err, err)
		})
	}
}
