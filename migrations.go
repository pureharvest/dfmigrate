package dfmigrate

import "fmt"

type Migration struct {
	Name      string
	Version   uint64
	Direction Direction
	Tables    []TableMigration
	SQL       []string
	Programs  []string
	Data      []DataMigration
}

func (m Migration) Filename() string {
	if m.Direction == Up {
		return fmt.Sprintf("%d_%s.up.yml", m.Version, m.Name)
	}

	return fmt.Sprintf("%d_%s.down.yml", m.Version, m.Name)

}

type SQLTableGenerator interface {
	Generate(table TableMigration) ([]string, error)
}

func buildInMemoryModel(migrationsPath string, version uint64) (Model, error) {
	var files MigrationFiles
	var model Model

	var err error

	if files, err = ReadMigrationFiles(migrationsPath); err != nil {
		return model, err
	}

	migrations, err := files.From(0, version)

	if err != nil {
		return model, err
	}
	return BuildModelFromMigrations(migrations)
}
