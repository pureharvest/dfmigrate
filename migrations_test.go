package dfmigrate

import (
	"testing"

	"gitlab.com/pureharvest/df32"

	"github.com/ghodss/yaml"
	"github.com/stretchr/testify/assert"
)

func Test_YAMLDefaultsTableMigration(t *testing.T) {
	tests := []struct {
		name     string
		input    []byte
		expected TableMigration
	}{
		{
			name: "all default",
			input: []byte(`
name: BLINE
file_num: 85
`),
			expected: TableMigration{
				Name:   "BLINE",
				Number: 85,
			},
		},
		{
			name: "",
			input: []byte(`
action: create
name: BLINE
file_num: 85
recnum: true
schema: test
display_name: Barcode Scanning Line Item File
fields:
- name: NUMBER
  type: ascii
  size: "10"
  use_index: 1
- name: DATE
  type: date
  size: "6"
indexes:
- name: BLINE001_PK
  clustered: true
  fields:
  - name: BARCODE
    descending: NO
    uppercase: YES
`),
			expected: TableMigration{
				Action:      "create",
				Number:      85,
				Name:        "BLINE",
				DisplayName: "Barcode Scanning Line Item File",
				Fields: []FieldMigration{
					{
						Name:   "NUMBER",
						Type:   "ascii",
						Size:   "10",
						Length: 10,
						Index:  1,
					},
					{
						Name:   "DATE",
						Type:   "date",
						Size:   "6",
						Length: 6,
					},
				},
				Indexes: []IndexMigration{
					{
						Name: "BLINE001_PK",
						Fields: []IndexField{
							{Name: "BARCODE", Uppercase: true},
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			table := TableMigration{}

			if err := yaml.Unmarshal(test.input, &table); err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected, table)
		})
	}
}

func Test_YAMLSQLMigration(t *testing.T) {
	tests := []struct {
		name     string
		input    []byte
		expected Migration
	}{
		{
			name: "all default",
			input: []byte(`
---
sql: 
- |
    Test multiple lines
    asdasd
    aaa

`),
			expected: Migration{
				SQL: []string{`Test multiple lines
asdasd
aaa
`},
			},
		},
		{
			name: "multiple statements",
			input: []byte(`
---
sql: 
- |
    Test multiple lines
    asdasd
    aaa
- statement 2
`),
			expected: Migration{
				SQL: []string{`Test multiple lines
asdasd
aaa
`, "statement 2"},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			migration := Migration{}

			err := yaml.Unmarshal(test.input, &migration)
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected, migration)
		})
	}
}

func Test_ModifyFields(t *testing.T) {
	tests := []struct {
		name      string
		from      df32.Field
		migration FieldMigration
		expected  df32.Field
	}{
		{
			name: "rename",
			from: df32.Field{
				Name:   "TEST",
				Type:   "ascii",
				Length: 10,
			},
			migration: FieldMigration{
				NewName: "TEST2",
			},
			expected: df32.Field{
				Name:   "TEST2",
				Type:   "ascii",
				Length: 10,
			},
		},
		{
			name: "change type",
			from: df32.Field{
				Name:   "TEST",
				Type:   "ascii",
				Length: 10,
			},
			migration: FieldMigration{
				Type: "number",
			},
			expected: df32.Field{
				Name:   "TEST",
				Type:   "number",
				Length: 10,
			},
		},
		{
			name: "change size",
			from: df32.Field{
				Name:   "TEST",
				Type:   "ascii",
				Length: 10,
			},
			migration: FieldMigration{
				Length: 20,
			},
			expected: df32.Field{
				Name:   "TEST",
				Type:   "ascii",
				Length: 20,
			},
		},
		{
			name: "change relation",
			from: df32.Field{
				Name:   "TEST",
				Type:   "ascii",
				Length: 10,
			},
			migration: FieldMigration{
				RelatesTo: df32.FieldRelation{
					File:  12,
					Field: 10,
				},
			},
			expected: df32.Field{
				Name:   "TEST",
				Type:   "ascii",
				Length: 10,
				RelatesTo: df32.FieldRelation{
					File:  12,
					Field: 10,
				},
			},
		},
		{
			name: "change multiple",
			from: df32.Field{
				Name:   "TEST",
				Type:   "ascii",
				Length: 10,
			},
			migration: FieldMigration{
				NewName: "TEST2",
				Length:  20,
			},
			expected: df32.Field{
				Name:   "TEST2",
				Type:   "ascii",
				Length: 20,
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			field := modifyField(test.from, test.migration)

			assert.Equal(t, test.expected, field)
		})
	}
}

func Test_PrepareCreateMigration(t *testing.T) {
	tests := []struct {
		name   string
		input  TableMigration
		output TableMigration
	}{
		{
			name: "sets field numbers",
			input: TableMigration{
				Fields: []FieldMigration{
					{Name: "FIELD1"},
					{Name: "FIELD2"},
					{Name: "FIELD3"},
				},
			},
			output: TableMigration{
				Fields: []FieldMigration{
					{Number: 1, Name: "FIELD1"},
					{Number: 2, Name: "FIELD2"},
					{Number: 3, Name: "FIELD3"},
				},
			},
		},
		{
			name: "sets index numbers",
			input: TableMigration{
				Indexes: []IndexMigration{
					{Name: "Index1"},
					{Name: "Index2"},
					{Name: "Index3"},
				},
			},
			output: TableMigration{
				Indexes: []IndexMigration{
					{Number: 1, Name: "Index1"},
					{Number: 2, Name: "Index2"},
					{Number: 3, Name: "Index3"},
				},
			},
		},
		{
			name: "sets index field numbers",
			input: TableMigration{
				Fields: []FieldMigration{
					{Name: "FIELD1"},
					{Name: "FIELD2"},
					{Name: "FIELD3"},
				},
				Indexes: []IndexMigration{
					{
						Name: "Index1",
						Fields: []IndexField{
							IndexField{Name: "FIELD1"},
						},
					},
				},
			},
			output: TableMigration{
				Fields: []FieldMigration{
					{Number: 1, Name: "FIELD1"},
					{Number: 2, Name: "FIELD2"},
					{Number: 3, Name: "FIELD3"},
				},
				Indexes: []IndexMigration{
					{
						Name:   "Index1",
						Number: 1,
						Fields: []IndexField{
							IndexField{Number: 1, Name: "FIELD1"},
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			migration := prepareCreateMigration(test.input)

			assert.Equal(t, test.output, migration)
		})
	}
}

func Test_FieldUnmarshal(t *testing.T) {
	tests := []struct {
		name     string
		data     string
		expected FieldMigration
		err      error
	}{
		{
			"basic",
			`
---
name: NUMBER
type: ascii
size: "10"`,
			FieldMigration{
				Name:      "NUMBER",
				Type:      df32.AsciiType,
				Size:      "10",
				Length:    10,
				Precision: 0,
			},
			nil,
		},
		{
			"length and precision",
			`
---
name: NUMBER
type: number
size: "10.3"`,
			FieldMigration{
				Name:      "NUMBER",
				Type:      df32.NumberType,
				Size:      "10.3",
				Length:    13,
				Precision: 3,
			},
			nil,
		},
		{
			"size as int",
			`
---
name: NUMBER
type: number
size: 10`,
			FieldMigration{
				Name:      "NUMBER",
				Type:      df32.NumberType,
				Size:      "10",
				Length:    10,
				Precision: 0,
			},
			nil,
		},
		{
			"size as float",
			`
---
name: NUMBER
type: number
size: 10.3`,
			FieldMigration{
				Name:      "NUMBER",
				Type:      df32.NumberType,
				Size:      "10.3",
				Length:    13,
				Precision: 3,
			},
			nil,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			field := FieldMigration{}

			err := yaml.Unmarshal([]byte(test.data), &field)

			if err != test.err {
				t.Fatal("unexpected error", err)
			}

			assert.Equal(t, test.expected, field)
		})
	}
}
