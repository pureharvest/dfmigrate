package dfmigrate

import (
	"fmt"
	"strings"

	"gitlab.com/pureharvest/df32"
)

func NewModel(version uint64) Model {
	model := Model{
		version: version,
		tables:  make([]*df32.Table, 4092),
	}

	return model
}

type Model struct {
	version uint64
	tables  []*df32.Table
}

func compareTable(a, b df32.Table) []string {
	var errs []string

	if a.Name != b.Name {
		errs = append(errs, fmt.Sprintf("Name: %s != %s", a.Name, b.Name))
	}

	if a.DisplayName != b.DisplayName {
		errs = append(errs, fmt.Sprintf("DisplayName: %s != %s", a.DisplayName, b.DisplayName))
	}
	if a.System != b.System {
		errs = append(errs, fmt.Sprintf("System: %t != %t", a.System, b.System))
	}

	for i, field := range a.Fields {

		if i >= len(b.Fields) {
			errs = append(errs, fmt.Sprintf("\tField missing %s.", field.Name))
			continue
		}

		if e := compareField(field, b.Fields[i]); len(e) != 0 {
			errs = append(errs, e...)
		}
	}

	return errs
}

func compareField(a, b df32.Field) []string {
	var errs []string

	if a.Name != b.Name {
		errs = append(errs, fmt.Sprintf("\tField Name %s != %s", a.Name, b.Name))
	}

	if a.Number != b.Number {
		errs = append(errs, fmt.Sprintf("\tField %s.Number: %d != %d", a.Name, a.Number, b.Number))
	}

	if a.Type != b.Type {
		errs = append(errs, fmt.Sprintf("\tField %s.Type: %s != %s", a.Name, a.Type, b.Type))
	}

	if a.Length != b.Length {
		errs = append(errs, fmt.Sprintf("\tField %s.Length: %d != %d", a.Name, a.Length, b.Length))
	}

	if a.Precision != b.Precision {
		errs = append(errs, fmt.Sprintf("\tField %s.Precision: %d != %d", a.Name, a.Precision, b.Precision))
	}

	return errs
}

func (m *Model) Compare(to Model) error {
	var errs []string

	if m.Version() != to.Version() {
		return fmt.Errorf("Different Versions: %d != %d", m.Version(), to.Version())
	}

	for _, table := range m.tables {
		if table != nil {
			table2, found := to.Get(int(table.Number))
			if !found {
				errs = append(errs, fmt.Sprintf("Table %d: %s not found", table.Number, table.Name))
				continue
			}

			if err := compareTable(*table, table2); len(err) != 0 {
				errs = append(errs, fmt.Sprintf("Table %s doesn't match:", table.Name))
				errs = append(errs, err...)
			}
		}

	}

	if len(errs) != 0 {
		return fmt.Errorf("%s", strings.Join(errs, "\n"))
	}

	return nil

}

func (m *Model) Set(i int, table df32.Table) {
	if i < 1 || i > len(m.tables) {
		return
	}

	m.tables[i-1] = &table
}

func (m Model) Get(i int) (df32.Table, bool) {
	if i < 1 || i > len(m.tables) {
		return df32.Table{}, false
	}

	table := m.tables[i-1]

	if table == nil {
		return df32.Table{}, false
	}

	return *table, true

}

func (m *Model) GetTableByName(name string) (df32.Table, bool) {
	for _, table := range m.tables {
		if table != nil && table.Name == name {
			return *table, true
		}
	}

	return df32.Table{}, false
}

func (m *Model) Version() uint64 {
	return m.version
}

func (m *Model) Tables() []df32.Table {
	tables := make([]df32.Table, 0, 4092)

	for _, table := range m.tables {
		if table != nil {
			tables = append(tables, *table)
		}
	}

	return tables
}
